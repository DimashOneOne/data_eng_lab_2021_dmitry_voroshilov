-- 1.	Distribution and dynamics of revenue by sales channels

COPY 
(SELECT EXTRACT (YEAR FROM time_id) AS "year", 
        channel_desc AS "channel name",
        round(sum(amount_sold) / 1000, 0) AS revenue
FROM sh.profits
INNER JOIN sh.channels USING (channel_id)
GROUP BY "year", channel_id, channel_desc)
TO 'D:\\sales.csv' DELIMITER ';' CSV HEADER;


-- 2.	Revenue and profit dynamics

COPY 
(SELECT EXTRACT (YEAR FROM time_id) AS "year", 
        round(sum(amount_sold) / 1000, 0) AS revenue, 
        round(sum(amount_sold - total_cost) / 1000, 0) AS profit
FROM sh.profits
GROUP BY "year")
TO 'D:\\sales2.csv' DELIMITER ';' CSV HEADER;


-- 3. Sales geography

COPY 
(SELECT country_name,
	    sum(amount_sold) AS revenue
FROM sh.countries
INNER JOIN sh.customers USING (country_id)
INNER JOIN sh.sales USING (cust_id)
GROUP BY country_id, country_name)
TO 'D:\\sales3.csv' DELIMITER ';' CSV HEADER; 