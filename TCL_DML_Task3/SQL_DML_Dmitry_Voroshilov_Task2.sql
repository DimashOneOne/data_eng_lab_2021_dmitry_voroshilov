-- 1. Create table �table_to_delete� and fill it with the following query

CREATE TABLE table_to_delete AS
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x; 


-- 2. Lookup how much space this table consumes with the following query

SELECT *, pg_size_pretty(total_bytes) AS total, 
		  pg_size_pretty(index_bytes) AS INDEX,
		  pg_size_pretty(toast_bytes) AS toast,
		  pg_size_pretty(table_bytes) AS TABLE
FROM (SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes
FROM (SELECT c.oid,nspname AS table_schema, 
			 relname AS TABLE_NAME,
			 c.reltuples AS row_estimate,
			 pg_total_relation_size(c.oid) AS total_bytes,
			 pg_indexes_size(c.oid) AS index_bytes,
			 pg_total_relation_size(reltoastrelid) AS toast_bytes
	 FROM pg_class c
	 LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
	 WHERE relkind = 'r'
             ) a
     ) a
WHERE table_name LIKE '%table_to_delete%';

/* oid = 17,289; table_schema = public; table_name = table_to_delete; 
   row_estimate = 10,000,080; total_bytes = 602,562,560; index_bytes = 0;
   toast_bytes = 8,192; table_bytes = 602,554,368; ABC total = 575 MB,
   ABC INDEX = 0 bytes; ABC toast = 8192 bytes; ABC TABLE = 575 MB */


-- 3. Issue the following DELETE operation on �table_to_delete

DELETE FROM table_to_delete
WHERE REPLACE(col, 'veeeeeeery_long_string','')::int % 3 = 0; 


-- a. Note how much time it takes to perform this DELETE statement = 20.508s

-- b. Lookup how much space this table consumes after previous DELETE: the same as it was;

-- c. Perform the following command (if you're using DBeaver, press Ctrl+Shift+O to observe server output (VACUUM results)):

VACUUM FULL VERBOSE table_to_delete;

-- d. �heck space consumption of the table once again and make conclusions;

/* oid = 17,289; table_schema = public; table_name = table_to_delete; 
   row_estimate = 6,666,667; total_bytes = 401,620,992; index_bytes = 0;
   toast_bytes = 8,192; table_bytes = 401,612,800; ABC total = 383 MB,
   ABC INDEX = 0 bytes; ABC toast = 8192 bytes; ABC TABLE = 383 MB 
   
   Space consumption was reduced.

*/

-- e. Recreate �table_to_delete� table;

DROP TABLE public.table_to_delete;

CREATE TABLE table_to_delete AS
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x; 


-- 4. Issue the following TRUNCATE operation:

TRUNCATE table_to_delete;

-- a. Note how much time it takes to perform this TRUNCATE statement = 62 ms

-- b. Compare with previous results and make conclusion: more faster

-- c. Check space consumption of the table once again and make conclusions;

/* oid = 17,319; table_schema = public; table_name = table_to_delete; 
   row_estimate = 0; total_bytes = 8,192; index_bytes = 0;
   toast_bytes = 8,192; table_bytes = 8,192; ABC total = 8192 bytes,
   ABC INDEX = 0 bytes; ABC toast = 8192 bytes; ABC TABLE = 0 bytes 
   
   There is no space significant consumption space
*/


-- 5. Hand over your investigation's results to your trainer. The results must include

-- a. Space consumption of �table_to_delete� table before and after each operation: as described above

-- b. Duration of each operation (DELETE, TRUNCATE): as described above