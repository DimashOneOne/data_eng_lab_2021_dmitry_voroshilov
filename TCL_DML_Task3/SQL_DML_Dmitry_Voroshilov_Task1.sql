-- Choose your top-3 favorite movies and add them to 'film' table. Fill rental rates with 4.99, 9.99 and 19.99 and rental durations with 1, 2 and 3 
-- weeks respectively.

WITH cte AS (SELECT *
			 FROM (SELECT upper('A zori zdes tikhie...') AS title, 
			                    1 AS language_id, 
			                    1 AS rental_duration, 
			                    4.99 AS rental_rate, 
			                    now() AS last_update -- I think, that here and further this is not nessery, becuse tables in 'last_update' have got this function as a default value
                  UNION ALL
				  SELECT upper('Groundhog Day'), 
				         1, 
				         2, 
				         9.99, 
				         now()
				  UNION ALL
			      SELECT upper('Jodaeiye Nader az Simin'), 
			             1, 
			             3, 
			             19.99, 
			             now()
			        ) t),
	cte2 AS (INSERT INTO public.film (title, language_id, rental_duration, rental_rate, last_update) 
	         SELECT title, 
	                language_id, 
	                rental_duration, 
	                rental_rate, 
	                last_update
	         FROM cte
	         WHERE NOT EXISTS (SELECT 1 
	                           FROM public.film f 
	                           WHERE cte.title = f.title )
			 RETURNING film_id, title, rental_rate)
SELECT * -- I've defined names of columns in RETURNING
INTO TEMP ins_f_t
FROM cte2;

         
/* 
   Next you can see my first variant. Then I've decided to check titles on uniqueness for this part of task 1. 
   I know that films can have similiar titels. Also there is no unique index in film table and I can't use 'on conflict' clause. 
   However, according to my hypothesis, the application should check this. So I've made the second variant shown above.
   
WITH ins_f AS (INSERT INTO public.film (title, language_id, rental_duration, rental_rate, last_update) VALUES
			  (upper('A zori zdes tikhie...'), 1, 1, 4.99, now()),
			  (upper('Groundhog Day'), 1, 2, 9.99, now()),
			   (upper('Jodaeiye Nader az Simin'), 1, 3, 19.99, now())
			   RETURNING film_id, title, rental_rate)
SELECT * -- I defined names of columns in RETURNING
INTO TEMP ins_f_t
FROM ins_f;
 */


-- Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total).

WITH ins_act AS (INSERT INTO public.actor (first_name, last_name, last_update) VALUES
	(upper('Pyotr'), upper('Fyodorov'), now()),
	(upper('Anastasiya'), upper('Mikulchina'), now()),
	(upper('Bill'), upper('Murray'), now()),
	(upper('Andy'), upper('MacDowell'), now()),
	(upper('Payman'), upper('Maadi'), now()),
	(upper('Leila'), upper('Hatami'), now())
RETURNING *)
INSERT INTO public.film_actor (actor_id, film_id, last_update) 
SELECT actor_id, 
	   film_id, 
	   now() AS last_update 
FROM ins_act ia CROSS JOIN LATERAL (SELECT film_id 
				   				    FROM ins_f_t
				   				    WHERE title = (CASE WHEN ((ia.first_name = upper('Pyotr') AND last_name = upper('Fyodorov')) OR
				   				       							(ia.first_name = upper('Anastasiya') AND last_name = upper('Mikulchina')))
				   				       					THEN upper('A zori zdes tikhie...')
				   				       					WHEN ((ia.first_name = upper('Bill') AND last_name = upper('Murray')) OR
				   				       							(ia.first_name = upper('Andy') AND last_name = upper('MacDowell')))
				   				       					THEN upper('Groundhog Day')
				   				       					WHEN ((ia.first_name = upper('Payman') AND last_name = upper('Maadi')) OR
				   				       							(ia.first_name = upper('Leila') AND last_name = upper('Hatami')))
				   				       				    THEN upper('Jodaeiye Nader az Simin')
				   				       			    END)) id_from_ins_f_t;
	

--  Add your favorite movies to any store's inventory

WITH cte AS (INSERT INTO public.inventory (film_id, store_id, last_update)
		 	 SELECT film_id,
			 	    store_id,
				    now() AS last_update 
		 	 FROM ins_f_t, store
			 RETURNING inventory_id, store_id, film_id)
SELECT * -- I've defined names of columns in RETURNING
INTO TEMP ins_i_t
FROM cte;

                           
-- Alter any existing customer in the database who has at least 43 rental and 43 payment records. Change his/her personal data to yours (first name, 
-- last name, address, etc.). Do not perform any updates on 'address' table, as it can impact multiple records with the same address. Change 
-- customer's create_date value to current_date.

WITH cte0 AS (INSERT INTO public.city (city, country_id)
             SELECT 'Gomel' AS city,
                     country_id
             FROM public.country
             WHERE country = 'Belarus' AND NOT EXISTS (SELECT 1 
                                                       FROM public.city c 
                                                       WHERE c.city = 'Gomel')
             RETURNING city_id),
	cte1 AS (SELECT city_id 
		     FROM cte0  
			 UNION 
	         SELECT city_id 
	         FROM public.city c 
	         WHERE c.city = 'Gomel'), --  For case when City is already in public.city (like Mogiljov).
	cte2 AS (INSERT INTO public.address (address, district, city_id, postal_code, phone)
			 SELECT 'Klermon-Ferran st., 17' AS address,
			        'Gomelski' AS district,
			         city_id,
			        '246027' AS postal_code,
			        '+375297315627' AS phone
			 FROM cte1
			 RETURNING address_id),
	cte3 AS (UPDATE public.customer 
		  	 SET first_name = 'Dmitry',
			     last_name = 'Voroshilov',
			     email = 'voroshilov@live.ru',
			     create_date = now(),
			     address_id = (SELECT *
			     	 		   FROM cte2)
			 WHERE customer_id = (SELECT customer_id
								  FROM payment
								  GROUP BY customer_id
								  HAVING count(payment_id) >= 43 AND 
								         count(DISTINCT payment_id) >= 43
							      LIMIT 1)
			 RETURNING customer_id)
SELECT * -- I've defined names of columns in RETURNING
INTO TEMP my_id
FROM cte3;

  
-- Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'

DELETE FROM public.payment
USING my_id
WHERE payment.customer_id = my_id.customer_id;

DELETE FROM public.rental
USING my_id
WHERE rental.customer_id = my_id.customer_id;


-- Rent you favorite movies from the store they are in and pay for them (add corresponding records to the database to represent this activity)
-- (Note: to insert the payment_date into the table payment, you can create a new partition (see the scripts to install the training database ) or add records for the 
-- first half of 2017)

CREATE TABLE public.payment_p2021_04 PARTITION OF public.payment
FOR VALUES FROM ('2021-04-01') TO ('2021-04-30');

WITH cte AS (INSERT INTO public.rental (rental_date, inventory_id, customer_id, staff_id, last_update)
			 SELECT now() AS rental_date,
			 	    inventory_id,
				    (SELECT * FROM my_id) AS customer_id,
				    staff_id,
				    now() AS last_update
			 FROM ins_i_t CROSS JOIN LATERAL (SELECT staff_id
										      FROM public.staff st
										      WHERE ins_i_t.store_id = st.store_id
										      LIMIT 1) t
			ON CONFLICT (rental_date, inventory_id, customer_id) DO NOTHING  
			RETURNING rental_id, inventory_id, customer_id, staff_id)
INSERT INTO public.payment_p2021_04 (customer_id, staff_id, rental_id, amount, payment_date)
SELECT customer_id,
	   staff_id,
	   rental_id,
	   amount,
	   now() AS payment_date
FROM cte INNER JOIN ins_i_t USING (inventory_id)
	   	 CROSS JOIN LATERAL (SELECT rental_rate AS amount
							 FROM ins_f_t
							 WHERE ins_f_t.film_id = ins_i_t.film_id) t;
