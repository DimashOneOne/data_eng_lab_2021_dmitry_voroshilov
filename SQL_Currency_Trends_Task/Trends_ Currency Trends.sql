-- temp table of zodiac signs

WITH cte AS (SELECT 'Aries' AS sign_name,
					21 AS start_day,
				    3 AS start_month,
				    20 AS finish_day,
				    4 AS finish_month
			UNION ALL
			SELECT 'Taurus',  21, 4, 21, 5
			UNION ALL
			SELECT 'Gemini',  22, 5, 21, 6
			UNION ALL
			SELECT 'Cancer',  22, 6, 22, 7
			UNION ALL
			SELECT 'Leo',     23, 7, 21, 8
			UNION ALL
			SELECT 'Virgo',   22, 8, 23, 9
			UNION ALL
			SELECT 'Libra',   24, 9, 23, 10
			UNION ALL
			SELECT 'Scorpio', 24, 10, 22, 11
			UNION ALL
			SELECT 'Sagittarius', 23, 11, 22, 12
			UNION ALL
			SELECT 'Capricorn', 23, 12, 20, 1
			UNION ALL
			SELECT 'Aquarius', 21, 1, 19, 2
			UNION ALL
			SELECT 'Pisces', 20, 2, 20, 3)
SELECT * -- I've defined names of columns in cte
INTO TEMP sings
FROM cte;


-- temp table of astro seasons

WITH cte AS (SELECT 'astro winter' AS season_name,
					22 AS start_day,
				    12 AS start_month,
				    20 AS finish_day,
				    3 AS finish_month
			UNION ALL
			SELECT 'astro spring',  21, 3, 20, 6
			UNION ALL
			SELECT 'astro summer',  21, 6, 22, 9
			UNION ALL
			SELECT 'astro autumn',  23, 9, 21, 12)
SELECT * -- I've defined names of columns in cte
INTO TEMP astro_seasons
FROM cte;


-- instability in zodiac signs

WITH cte AS (SELECT er_date, 
                    er_value, 
                    sign_name
		 	 FROM currency.byn_usd_exchange_rate INNER JOIN sings ON (EXTRACT(DAY FROM er_date) >= start_day AND
		 	                                                         EXTRACT(MONTH FROM er_date) = start_month) OR 
											                         (EXTRACT(DAY FROM er_date) <= finish_day AND 
											                         EXTRACT(MONTH FROM er_date) = finish_month))
SELECT t.sign_name, 
       round(avg(abs(1-(t.er_value/c1.er_value))), 4) AS avg_instability, 
       round(max(t.er_value/c1.er_value), 4) AS max_growt_rate, 
       round(min(t.er_value/c1.er_value), 4) AS min_growt_rate
FROM cte c1 CROSS JOIN LATERAL (SELECT sign_name,
									   er_value 
                                FROM cte c2
                                WHERE c1.er_date + 1 = c2.er_date) t
GROUP BY t.sign_name
ORDER BY avg_instability DESC; 


-- instability in astro seasons
								
WITH cte AS (SELECT er_date, 
                    er_value, 
                    season_name
		 	 FROM currency.byn_usd_exchange_rate INNER JOIN astro_seasons ON (EXTRACT(DAY FROM er_date) >= start_day AND
		 	                                                              EXTRACT(MONTH FROM er_date) = start_month) OR 
											                              (EXTRACT(DAY FROM er_date) <= finish_day AND 
											                              EXTRACT(MONTH FROM er_date) = finish_month))
SELECT t.season_name, 
       round(avg(abs(1-(t.er_value/c1.er_value))), 4) AS avg_instability, 
       round(max(t.er_value/c1.er_value), 4) AS max_growt_rate, 
       round(min(t.er_value/c1.er_value), 4) AS min_growt_rate
FROM cte c1 CROSS JOIN LATERAL (SELECT season_name,
									   er_value 
                                FROM cte c2
                                WHERE c1.er_date + 1 = c2.er_date) t
GROUP BY t.season_name
ORDER BY avg_instability DESC; 


-- correlation with the Earth's rotation

WITH cte AS (SELECT er_value,
			 	    (SELECT count(1) 
			 	    FROM currency.byn_usd_exchange_rate er2 
			 	    where er1.er_date >= er2.er_date + 1) rotate_count
		 	 FROM currency.byn_usd_exchange_rate er1)
SELECT corr(er_value, rotate_count) AS "correlation coefficient",
       regr_r2(er_value, rotate_count) AS "average of the independent variable"
FROM cte;