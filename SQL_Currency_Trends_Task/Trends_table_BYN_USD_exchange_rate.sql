CREATE SCHEMA IF NOT EXISTS currency;

CREATE TABLE IF NOT EXISTS currency.BYN_USD_exchange_rate (
  er_date date NOT NULL,
  er_value numeric(9,4) NOT NULL,
  UNIQUE (er_date));
  

/* COPY currency.BYN_USD_exchange_rate FROM 'D:\\currency.csv' DELIMITER ';' CSV HEADER; */