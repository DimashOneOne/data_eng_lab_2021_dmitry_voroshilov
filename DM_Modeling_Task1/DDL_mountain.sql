CREATE TABLE IF NOT EXISTS `mydb`.`mountain` (
  `mountain_ID` SMALLINT UNSIGNED NOT NULL,
  `mountain_name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`mountain_ID`),
  INDEX `IDX_mountain_name` (`mountain_name` ASC) VISIBLE);
