CREATE TABLE IF NOT EXISTS `mydb`.`peak` (
  `peak_ID` SMALLINT UNSIGNED NOT NULL,
  `peak_name` VARCHAR(150) NOT NULL,
  `peak_height` SMALLINT NOT NULL,
  `mountain_ID` SMALLINT UNSIGNED NULL,
  `location_ID` SMALLINT UNSIGNED NULL,
  PRIMARY KEY (`peak_ID`),
  INDEX `FK_peak_mountain_ID` (`mountain_ID` ASC) VISIBLE,
  INDEX `FK_peak_location_ID` (`location_ID` ASC) VISIBLE,
  CONSTRAINT `FK_mountain_mountain_ID_mountain_ID`
    FOREIGN KEY (`mountain_ID`)
    REFERENCES `mydb`.`mountain` (`mountain_ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_location_location_ID_location_ID`
    FOREIGN KEY (`location_ID`)
    REFERENCES `mydb`.`location` (`location_ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
