CREATE TABLE IF NOT EXISTS `mydb`.`m2m_climbing` (
  `climbing_group_ID` SMALLINT UNSIGNED NOT NULL,
  `alpinist_ID` INT UNSIGNED NOT NULL,
  `m2m_climbing_success_indicator` BIT(1) NULL,
  INDEX `FK_m2m_climbing_alpinist_ID` (`alpinist_ID` ASC) INVISIBLE,
  INDEX `FK_m2m_climbing_climbing_group_ID` (`climbing_group_ID` ASC) VISIBLE,
  PRIMARY KEY (`climbing_group_ID`, `alpinist_ID`),
  CONSTRAINT `FK_alpinist_alpinist_ID_alpinist_ID`
    FOREIGN KEY (`alpinist_ID`)
    REFERENCES `mydb`.`alpinist` (`alpinist_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_climbing_group_climbing_group_ID_climbing_group_ID`
    FOREIGN KEY (`climbing_group_ID`)
    REFERENCES `mydb`.`climbing_group` (`climbing_group_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
