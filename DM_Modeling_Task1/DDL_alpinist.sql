CREATE TABLE IF NOT EXISTS `mydb`.`alpinist` (
  `alpinist_ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `alpinist_name` VARCHAR(150) NOT NULL,
  `location_ID` SMALLINT UNSIGNED NULL,
  PRIMARY KEY (`alpinist_ID`),
  INDEX `FK_apinist_location_ID` (`location_ID` ASC) VISIBLE,
  CONSTRAINT `FK_location_location_ID_location_ID_2`
    FOREIGN KEY (`location_ID`)
    REFERENCES `mydb`.`location` (`location_ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
