CREATE TABLE IF NOT EXISTS `mydb`.`climbing_group` (
  `climbing_group_ID` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `climbing_group_name` VARCHAR(150) NULL,
  `climbing_group_start_date` DATE NULL,
  `climbing_group_finish_date` DATE NULL,
  `peak_ID` SMALLINT UNSIGNED NULL,
  PRIMARY KEY (`climbing_group_ID`),
  INDEX `FK_climbing_group_peak_ID` (`peak_ID` ASC) VISIBLE,
  INDEX `IDX_climbing_group_start_date` (`climbing_group_start_date` ASC) VISIBLE,
  INDEX `IDX_climbing_group_finish_date` (`climbing_group_finish_date` ASC) VISIBLE,
  CONSTRAINT `FK_peak_peak_ID_peak_ID`
    FOREIGN KEY (`peak_ID`)
    REFERENCES `mydb`.`peak` (`peak_ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
