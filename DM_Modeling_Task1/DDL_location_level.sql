CREATE TABLE IF NOT EXISTS `mydb`.`location_level` (
  `location_level_ID` TINYINT NOT NULL AUTO_INCREMENT,
  `location_level_name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`location_level_ID`),
  UNIQUE INDEX `UQ_location_level_name` (`location_level_name` ASC) VISIBLE);
