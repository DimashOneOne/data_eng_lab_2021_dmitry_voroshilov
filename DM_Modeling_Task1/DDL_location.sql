CREATE TABLE IF NOT EXISTS `mydb`.`location` (
  `location_ID` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_parent_ID` SMALLINT UNSIGNED NULL,
  `location_name` VARCHAR(150) NOT NULL,
  `location_level_ID` TINYINT NOT NULL,
  PRIMARY KEY (`location_ID`),
  INDEX `RFK_location_parent_ID` (`location_parent_ID` ASC) VISIBLE,
  INDEX `FK_location_location_level_ID` (`location_level_ID` ASC) VISIBLE,
  CONSTRAINT `RFK_location_location_parent_ID_location_ID`
    FOREIGN KEY (`location_parent_ID`)
    REFERENCES `mydb`.`location` (`location_ID`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `FK_location_level_location_level_ID_location_level_ID`
    FOREIGN KEY (`location_level_ID`)
    REFERENCES `mydb`.`location_level` (`location_level_ID`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);
