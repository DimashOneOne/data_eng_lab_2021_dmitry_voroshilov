-- 1. Create a function that will return the most popular film for each country (where country is an input paramenter).
-- The function should return the result set in the following view: 
-- Query (example):select * from core.most_popular_films_by_countries(array['Afghanistan','Brazil','United States�])


CREATE OR REPLACE FUNCTION public.most_popular_films_by_countries(VARIADIC TEXT[], 
																  OUT "Country" country.country%TYPE, 
																  OUT "Film" film.title%TYPE, 
																  OUT "Rating" film.rating%TYPE,
																  OUT "Language" language.name%TYPE,
																  OUT "Length" film.length%TYPE,
																  OUT "Release year" film.release_year%TYPE)
RETURNS SETOF record
AS $$
WITH cte AS (SELECT DISTINCT film_id,
					country,
					country_id,
		        	title,
		        	rating,
		        	language.name,
		        	film.length,
		        	release_year,
		        	count(country) over(PARTITION BY country_id, film_id) AS rnt_cnt
			FROM (SELECT country,
					     country_id 
			      FROM public.country 
			      WHERE country = ANY ($1)) ct  
			INNER JOIN public.city USING (country_id)
			INNER JOIN public.address USING (city_id)
			INNER JOIN public.store USING (address_id)
			INNER JOIN public.inventory USING (store_id)
			INNER JOIN public.rental USING (inventory_id)
			INNER JOIN public.film USING (film_id)
			INNER JOIN public.language USING (language_id)
			ORDER BY rnt_cnt DESC)
SELECT country,
	   initcap(title),
	   rating,
	   cte.name,
	   cte.length,
	   release_year
FROM cte 
WHERE film_id = (SELECT cte2.film_id
				 FROM cte cte2
				 WHERE cte2.country_id = cte.country_id
				 LIMIT 1);
$$
LANGUAGE SQL;


-- DROP FUNCTION public.most_popular_films_by_countries(TEXT[]);


-- SELECT * FROM public.most_popular_films_by_countries('Australia', 'Canada');


-- SELECT * FROM public.most_popular_films_by_countries('Australia');


-- SELECT * FROM public.most_popular_films_by_countries('Afghanistan');


-- SELECT * FROM public.store;


CREATE OR REPLACE FUNCTION public.most_popular_films_by_countries_of_customers(VARIADIC TEXT[], 
																               OUT "Country" country.country%TYPE, 
																               OUT "Film" film.title%TYPE, 
																               OUT "Rating" film.rating%TYPE,
																               OUT "Language" language.name%TYPE,
																               OUT "Length" film.length%TYPE,
																               OUT "Release year" film.release_year%TYPE)
RETURNS SETOF record
AS $$
WITH cte AS (SELECT DISTINCT film_id,
					country,
					country_id,
		        	title,
		        	rating,
		        	language.name,
		        	film.length,
		        	release_year,
		        	count(country) over(PARTITION BY country_id, film_id) AS rnt_cnt
			FROM (SELECT country,
					     country_id 
			      FROM public.country 
			      WHERE country = ANY ($1)) ct  
			INNER JOIN public.city USING (country_id)
			INNER JOIN public.address USING (city_id)
			INNER JOIN public.customer USING (address_id)
			INNER JOIN public.rental USING (customer_id)
			INNER JOIN public.inventory USING (inventory_id)
			INNER JOIN public.film USING (film_id)
			INNER JOIN public.language USING (language_id)
			ORDER BY rnt_cnt DESC)
SELECT country,
	   initcap(title),
	   rating,
	   cte.name,
	   cte.length,
	   release_year
FROM cte 
WHERE film_id = (SELECT cte2.film_id
				 FROM cte cte2
				 WHERE cte2.country_id = cte.country_id
				 LIMIT 1);
$$
LANGUAGE SQL;


-- DROP FUNCTION public.most_popular_films_by_countries_of_customers(TEXT[]);


-- SELECT * FROM public.most_popular_films_by_countries_of_customers('Afghanistan', 'Brazil', 'United States');


-- SELECT * FROM public.most_popular_films_by_countries_of_customers('Australia');


-- SELECT * FROM public.most_popular_films_by_countries_of_customers('Afghanistan');


-- 2. Create a function that will return a list of films by part of the title in stock (for example, films with the word 'love' in the title).
--	� So, the title of films consists of �%...%�, and if a film with the title is out of stock, please return a message: a movie with that title was not found
--	� The function should return the result set in the following view (notice: row_num field is generated counter field (1,2, �, 100, 101, �))


CREATE OR REPLACE FUNCTION public.films_in_the_stock(text)
RETURNS TABLE ("Row_num" bigint,
			   "Film title" TEXT,
		--	   "Rating" mpaa_rating,
			   "Language" bpchar(20),
			   "Customer name" TEXT,
			   "Rental date" timestamptz)
AS $$
BEGIN 
	RETURN QUERY SELECT row_number() over() AS Row_num,
	             initcap(title),
	   --        rating,
	             language.name,
	             initcap(concat(first_name, ' ', last_name)),
	             rental_date
				 FROM (SELECT title,
				 			  rating,
				 			  language_id,
				 			  film_id
				 	   FROM public.film 
				       WHERE title ILIKE '%' || $1 || '%') AS t
				 INNER JOIN public.language USING (language_id)
				 INNER JOIN public.inventory USING (film_id)
				 INNER JOIN public.rental USING (inventory_id)
				 INNER JOIN public.customer USING (customer_id);
	IF NOT FOUND THEN 
	RAISE EXCEPTION 'A movie with that title was not found';
	END IF;
RETURN;
END 
$$
LANGUAGE plpgsql;

-- DROP FUNCTION films_in_the_stock(text);

-- SELECT * FROM public.films_in_the_stock('love');

-- SELECT * FROM public.films_in_the_stock('sdghkh');



-- 3. Create function that inserts new movie with the given name in �film� table. �release_year�, �language� are optional arguments and default to current year and 
-- Russian respectively. The function must return film_id of the inserted movie.


CREATE OR REPLACE FUNCTION public.insert_movie(IN TEXT, IN YEAR DEFAULT EXTRACT(YEAR FROM current_date), IN bpchar(20) DEFAULT 'Russian', OUT film_id bigint)
AS $$
WITH cte0 AS (INSERT INTO public.language (name)
              SELECT $3 AS name
              FROM public.language
              WHERE NOT EXISTS (SELECT 1 
                                FROM public.language l 
                                WHERE l.name = $3)
              LIMIT 1 
              RETURNING language_id),
	cte1 AS (SELECT language_id 
		     FROM cte0  
			 UNION 
	         SELECT language_id 
	         FROM public.language 
	         WHERE name = $3) --  For case when language is already in public.language
INSERT INTO public.film (title, release_year, language_id)
SELECT upper($1) AS title,
	   $2 AS release_year,
	   language_id
FROM cte1
RETURNING film_id;
$$
LANGUAGE SQL;


-- SELECT * FROM public.insert_movie('sdsd', 2015, 'English');

-- SELECT * FROM public.insert_movie('sdsd', 2015);

-- SELECT * FROM public.insert_movie('sdsd');
