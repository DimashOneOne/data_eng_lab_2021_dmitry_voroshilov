/*4. Prepare answers to the following questions:
� What operations do the following functions perform: film_in_stock, film_not_in_stock, inventory_in_stock, get_customer_balance, 
inventory_held_by_customer, rewards_report, last_day? You can find these functions in dvd_rental database. 

The film_in_stock is used to determine whether any copies of a given film are in stock at a given store by select and count operators.
The film_not_in_stock is used to determine whether there are any copies of a given film not in stock (rented out) at a given store by select and count operators.
The inventory_in_stock returns a boolean value indicating whether the inventory item specified is in stock by select, count, and if-else operators.
The get_customer_balance returns the current amount owing on a specified customer's account by select and sum operators.
The inventory_held_by_customer returns the customer_id of the customer who has rented out the specified inventory item by select operator.
Last_day returns the last day of the month for a given date by select and case operators.
The rewards_report generates a customizable list of the top customers for the previous month by  if, for operators.


� Why does �rewards_report� function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.

I don't have any problems with this function. It does not return 0 rows when the system time was set to 2017. */

CREATE OR REPLACE FUNCTION public.rewards_report_of_last_existing_data(min_monthly_purchases integer, min_dollar_amount_purchased numeric)  -- changed
 RETURNS SETOF customer
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
	last_payment_date DATE; -- new
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    /* Some sanity checks... */
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;
   
    SELECT max(payment_date) INTO last_payment_date  -- new
    FROM public.payment;

    last_month_start := last_payment_date - '3 month'::interval;   -- changed
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    /*
    Create a temporary storage area for Customer IDs.
    */
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);

    /*
    Find all customers meeting the monthly purchase requirements
    */

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;

    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
        RETURN NEXT rr;
    END LOOP;

    /* Clean up */
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$function$
;

-- SELECT * FROM public.rewards_report_of_last_existing_data(1, 1);

/*
� Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?

Functions might be delete: rewards_report, last_day, inventory_in_stock, inventory_held_by_customer, insert_movie, get_customer_balance, 
film_not_in_stock, film_in_stock. The last_updated and _group_concat functions might deleted using cascade, since other database objects depend on them. 
Functions can be deleted by their owner, who is the postgres user of the operating system who initializes the initial creation of the database.

� * The �get_customer_balance� function describes the business requirements for calculating the client balance. Unfortunately, not all of 
them are implemented in this function. Try to change function using the requirements from the comments. */

CREATE OR REPLACE FUNCTION public.get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
       --#OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
       --#THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
       --#   1) RENTAL FEES FOR ALL PREVIOUS RENTALS
       --#   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
       --#   3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
       --#   4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
DECLARE
    v_rentfees DECIMAL(5,2); --#FEES PAID TO RENT THE VIDEOS INITIALLY
    v_overfees INTEGER;      --#LATE FEES FOR PRIOR RENTALS
    v_payments DECIMAL(5,2); --#SUM OF PAYMENTS MADE PREVIOUSLY
    v_replacement DECIMAL(5,2); --#SUM OF THE REPLACEMENT_COST
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval)
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 -- * 1 dollar
                           ELSE 0
                        END),0) 
    INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM (CASE 
                           WHEN (rental.return_date - rental.rental_date) > 2 * film.rental_duration * '1 day'::interval
                           THEN film.replacement_cost
                           ELSE 0
                           END), 0) INTO v_replacement
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.return_date < p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_payments - (v_rentfees + v_overfees + v_replacement);
END
$function$
;

/*
� * How do �group_concat� and �_group_concat� functions work? (database creation script might help) Where are they used? �group_concat� can�t be used.

Group_concat-aggregating function that combines the values of strings separated by commas with a space. Used in the actor_info, film_list, nicer_but_slower_film_list views.

_group_concat is a state function for group_concat, which directly takes the values of the rows, combines them, and returns the final value

� * What does �last_updated� function do? Where is it used? 

Returns current timestamp for last_updated trigger.

� * What is tmpSQL variable for in �rewards_report� function? Can this function be recreated without EXECUTE statement and dynamic SQL?
tmpSQL is a variable with text type. I think yes.
*/
