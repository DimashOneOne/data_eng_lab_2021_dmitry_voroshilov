-- Task 1
-- Build the query to generate a report about the most significant customers (which have maximum sales) through various sales channels. 
-- The 5 largest customers are required for each channel.
-- Column sales_percentage shows percentage of customer�s sales within channel sales 

WITH cte AS (SELECT channel_id,
			        channel_desc,
			        cust_id,
				    cust_last_name,
				    cust_first_name,
				    sum(amount_sold) AS amount_sold,
				    sum(sum(amount_sold)) over(PARTITION BY channel_id) AS channel_amount,
				    sum(sum(amount_sold)) over(PARTITION BY channel_id, cust_id) AS customer_amount,
				    round(sum(sum(amount_sold)) over(PARTITION BY channel_id, cust_id) / sum(sum(amount_sold)) over(PARTITION BY channel_id) * 100, 5) AS sales_persentage,
				    rank() over(PARTITION BY channel_id ORDER BY sum(amount_sold) DESC) AS customer_rank
			 FROM sh.channels
			 INNER JOIN sh.sales USING (channel_id)
			 INNER JOIN sh.customers USING (cust_id)
			 GROUP BY channel_id, channel_desc, cust_id, cust_last_name, cust_first_name)
SELECT channel_desc,
	   cust_last_name,
	   cust_first_name,
	   amount_sold,
	   replace(sales_persentage::varchar, '0.', '.') || ' %' AS sales_persentage
FROM cte
WHERE customer_rank <= 5
ORDER BY channel_desc ASC, amount_sold DESC;


-- Task 2
-- Compose query to retrieve data for report with sales totals for all products in Photo category in Asia (use data for 2000 year). Calculate report total 
-- (YEAR_SUM).

-- This variant likes me more, but...

SELECT prod_name AS product_name,                    
	   sum(amount_sold) FILTER (WHERE calendar_quarter_number = 1) AS q1,
	   sum(amount_sold) FILTER (WHERE calendar_quarter_number = 2) AS q2,
	   sum(amount_sold) FILTER (WHERE calendar_quarter_number = 3) AS q3,
	   sum(amount_sold) FILTER (WHERE calendar_quarter_number = 4) AS q4,
	   sum(amount_sold) AS year_sum   
FROM sh.sales
INNER JOIN sh.customers USING (cust_id)
INNER JOIN sh.countries USING (country_id)
INNER JOIN sh.products USING (prod_id)
INNER JOIN sh.times USING (time_id)
WHERE prod_category = 'Photo' AND 
	  calendar_year = 2000 AND 
	  country_region = 'Asia'
GROUP BY prod_id, prod_name
ORDER BY prod_name;

-- * There are a lot of ways to get such report. You can try to use crosstab function. 

CREATE EXTENSION tablefunc;

WITH cte AS (SELECT *
			 FROM crosstab('SELECT prod_name,
							      DENSE_RANK() OVER(ORDER BY calendar_quarter_number),
								  sum(amount_sold)
						   FROM sh.sales
						   INNER JOIN sh.customers USING (cust_id)
						   INNER JOIN sh.countries USING (country_id)
						   INNER JOIN sh.products USING (prod_id)
						   INNER JOIN sh.times USING (time_id)
						   WHERE prod_category = ''Photo'' AND 
                                 calendar_year = 2000 AND 
                                 country_region = ''Asia''
						   GROUP BY calendar_quarter_number, prod_id, prod_name
						   ORDER BY 1, 2') AS �t (product_name varchar(50), q1 NUMERIC(10,2), q2 NUMERIC(10,2), q3 NUMERIC(10,2), q4 NUMERIC(10,2)))
SELECT product_name,
	   q1,
	   q2,
	   q3,
	   q4,
	   COALESCE(q1, 0) + COALESCE(q2, 0) + COALESCE(q3, 0) + COALESCE(q4,0) AS year_sum
FROM cte
ORDER BY product_name;


-- Task 3
-- Build the query to generate a report about customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001. This 
-- report should separate clients by sales channels, and, at the same time, channels should be calculated independently (i.e. only purchases made on 
-- selected channel are relevant).


WITH cte AS (SELECT channel_id,
                    channel_desc,
				    cust_id,
				    cust_last_name,
				    cust_first_name,
				    calendar_year,
				    sum(amount_sold) AS amount_sold,
				    rank() OVER (PARTITION BY channel_id, calendar_year ORDER BY sum(amount_sold) desc) AS customer_ranc
			 FROM sh.channels
			 INNER JOIN sh.sales USING (channel_id)
			 INNER JOIN sh.customers USING (cust_id)
			 INNER JOIN sh.times USING (time_id)
			 WHERE calendar_year IN (1998, 1999, 2001)
			 GROUP BY channel_id, channel_desc, cust_id, cust_last_name, cust_first_name, calendar_year)
SELECT channel_desc,
       cust_id,
       cust_last_name,
       cust_first_name,
	   sum(amount_sold) AS amount_sold
FROM cte
WHERE customer_ranc <= 300
GROUP BY channel_id, channel_desc, cust_id, cust_last_name, cust_first_name
HAVING count(1) = 3
ORDER BY amount_sold desc;


-- Task 4
-- Build the query to generate the report about sales in America and Europe:

SELECT calendar_month_desc,
       prod_category,                    
	   round(sum(amount_sold) FILTER (WHERE country_region = 'Americas'), 0) AS "Americas SALES",
	   round(sum(amount_sold) FILTER (WHERE country_region = 'Europe'), 0) AS "Europe SALES"
FROM sh.sales
INNER JOIN sh.customers USING (cust_id)
INNER JOIN sh.countries USING (country_id)
INNER JOIN sh.products USING (prod_id)
INNER JOIN sh.times USING (time_id)
WHERE calendar_month_desc IN ('2000-01', '2000-02', '2000-03') AND
      country_region IN ('Americas', 'Europe')
GROUP BY calendar_month_desc, prod_category_id, prod_category;