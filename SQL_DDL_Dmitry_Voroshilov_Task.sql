/*
1. Create database that monitors workload, capabilities and activities of our city's health institutions. 
The database needs to represent institutions, their locations, staffing, capacity, capabilities and patients' visits.
Constraints:
✓ 6+ tables
✓ 5+ rows in every table, 50+ rows total
✓ 3NF, Primary and Foreign keys must be defined
✓ Not null constraints where appropriate and at least 2 check constraints of other type
✓ Using DEFAULT and GENERATED ALWAYS AS are encouraged 
*/

CREATE DATABASE health_inst OWNER postgres;

-- CREATE EXTENSION btree_gist; -- installation of the additional module gist

-- DROP SCHEMA workflow CASCADE; 


/* There was no requirement to attach a model in the task. However this is https://prnt.sc/12aut6u alfa version of my datalogical model */

CREATE SCHEMA workflow;


CREATE DOMAIN var_names AS varchar(150) CHECK (char_length(trim(value)) > 0);   -- to prevent blank strings


CREATE TABLE workflow.position (
	pos_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	pos_name var_names NOT NULL UNIQUE
);


CREATE TABLE workflow.person (
	p_ID BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	p_name var_names NOT NULL  -- this data, like some other, is deliberately simplified in order to have time to implement other things
);


CREATE TABLE workflow.institution (
	inst_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	inst_name var_names NOT NULL,
	inst_address var_names NOT NULL,
	UNIQUE (inst_name, inst_address)
);


CREATE TABLE workflow.capability (
	ca_ID INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	ca_name var_names NOT NULL UNIQUE,
	ca_description TEXT NULL
);


CREATE TABLE workflow.institution_historical_capacity (
	inst_ID SMALLINT REFERENCES workflow.institution (inst_ID),
	ihc_date_from date CHECK (ihc_date_from >= '1900-01-01'),
	ihc_capacity INT NOT NULL CHECK (ihc_capacity > 0),
	PRIMARY KEY (inst_ID, ihc_date_from)
);


CREATE TABLE workflow.capability_potencial (
	inst_ID SMALLINT REFERENCES workflow.institution (inst_ID),
	ca_ID INT REFERENCES workflow.capability (ca_ID),
	cap_date_in DATE NOT NULL CHECK (cap_date_in  >= '1950-01-01'),
	cap_date_out DATE NULL CHECK (cap_date_out >= '1950-01-01'),
	cap_active_idicator BIT(1) NULL, -- or bit or boolean
	PRIMARY KEY (inst_ID, ca_ID),
	CHECK (cap_date_in <= cap_date_out),
	CHECK ((cap_date_out IS NOT NULL AND cap_active_idicator = '0') OR cap_date_out IS NULL)
);


CREATE TABLE workflow.staff (
	s_ID BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	p_ID BIGINT NOT NULL REFERENCES workflow.person (p_ID),
	inst_ID SMALLINT NOT NULL REFERENCES workflow.institution (inst_ID),
	pos_ID SMALLINT NOT NULL REFERENCES workflow.position (pos_ID),
	s_date_in DATE NOT NULL CHECK (s_date_in  >= '1930-01-01'),
	s_date_out DATE NULL CHECK (s_date_out  >= '1950-01-01'),
	s_active_idicator BIT(1) NOT NULL,
	CHECK (s_date_in <= s_date_out),
	CHECK ((s_date_out IS NOT NULL AND s_active_idicator = '0') OR s_date_out IS NULL)
);


CREATE TABLE workflow.visit (
	v_ID BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	p_ID BIGINT NOT NULL REFERENCES workflow.person (p_ID),
	s_ID BIGINT NOT NULL REFERENCES workflow.staff (s_ID),
	ca_ID INT NULL REFERENCES workflow.capability (ca_ID),
	v_plan_time TIMESTAMP NOT NULL CHECK (v_plan_time  >= '2000-01-01 00:00:00'),
    v_plan_interval INTERVAL NOT NULL DEFAULT ('10 minutes'),
	v_success_indicator BIT(1) NOT NULL,
	CHECK (p_ID != s_ID),
	EXCLUDE USING gist (p_ID WITH =, tsrange(v_plan_time , v_plan_time  + v_plan_interval, '[]') WITH &&)  -- to prevent similar records on same time range
);   


CREATE VIEW staff_underloading_3 AS
WITH cte AS (SELECT s_ID, 
				    p_name, 
				    EXTRACT (MONTH FROM v_plan_time) AS prev_month
			FROM workflow.staff INNER JOIN workflow.person pe USING (p_ID)
								INNER JOIN workflow.visit vi USING (s_ID)	    
			WHERE v_success_indicator = '1' AND 
				  tsrange(v_plan_time, v_plan_time  + v_plan_interval, '[]') && 
				  tsrange(date_trunc('month', now()::timestamp - '3 month'::INTERVAL), date_trunc('month', now()::timestamp), '[)') -- hypothesis: few months = 3
			GROUP BY s_ID, p_name, prev_month
			HAVING count(1) < 5)
SELECT s_ID, 
	   p_name
FROM cte 
GROUP BY s_ID, p_name
HAVING count(1) = 3; -- accounting only for those doctors who have had practice with patients for all three months


INSERT INTO workflow.position (pos_name)
VALUES (upper('хирург')),
       (upper('невролог')),
       (upper('инфекционист')),
       (upper('онколог')),
       (upper('психолог'));

      
INSERT INTO workflow.person (p_name)
VALUES (upper('ворошилов дмитрий евгеньевич')),
       (upper('полещук наталья сергеевна')),
       (upper('ворошилова анна дмитриевна')),
       (upper('ворошилов лев дмитриевич')),
       (upper('ворошилова евдокия савельевна'));
      

INSERT INTO workflow.institution (inst_name, inst_address)
VALUES (upper('Гомельская центральная городская поликлиника филиал №1'), upper('ул. Косарева, 11')),
       (upper('Гомельская центральная городская поликлиника филиал №2'), upper('ул. Бочкина, 182а')),
       (upper('Гомельская центральная городская поликлиника филиал №3'), upper('ул. Огоренко, 3')),
       (upper('Гомельская центральная городская поликлиника филиал №4'), upper('ул. Ландышева, 17')),
       (upper('Гомельская центральная городская поликлиника филиал №5'), upper('ул. Быховская, 106'));    
      

INSERT INTO workflow.capability (ca_name)
VALUES (upper('электрокардиограмма')),
       (upper('УЗИ печени')),
       (upper('рентгенография костей таза')),
       (upper('рентгенография костей желудка')),
       (upper('УЗИ мочевого пузыря'));

      
INSERT INTO workflow.institution_historical_capacity (inst_ID, ihc_date_from, ihc_capacity)
VALUES (1, '2000-01-01', 30000),
       (2, '2000-01-01', 30000),
       (3, '2000-01-01', 30000),
       (4, '2000-01-01', 30000),
       (1, '2010-01-01', 50000);

      
INSERT INTO workflow.capability_potencial (inst_ID, ca_ID, cap_date_in, cap_active_idicator)
VALUES (1, 1, '2000-01-01', '1'),
       (1, 4, '2000-01-01', '1'),
       (1, 2, '2000-01-01', '1'),
       (1, 3, '2000-01-01', '1'),
       (2, 4, '2000-01-01', '0');     
      
    
INSERT INTO workflow.staff (p_ID, inst_ID, pos_ID, s_date_in, s_active_idicator)
VALUES (1, 1, 1, '2000-01-01', '1'),
       (2, 2, 2, '2000-01-01', '1'),
       (3, 3, 3, '2000-01-01', '1'),
       (4, 4, 4, '2000-01-01', '1'),
       (4, 5, 4, '2010-01-01', '0');        


INSERT INTO workflow.visit (p_ID, s_ID, ca_ID, v_plan_time, v_success_indicator)
VALUES (1, 3, 1, '2021-01-03 12:20:00', '1'),
       (2, 3, null, '2021-02-03 10:10:00', '1'),
       (1, 3, 3, '2021-02-03 12:00:00', '1'),
       (4, 3, 4, '2021-03-04 12:00:00', '1'),
       (4, 1, NULL, '2021-01-04 12:30:00', '0'); 
     

-- 2. Write a query to identify doctors with insufficient workload (less than 5 patients a month for the past few months)

SELECT * FROM staff_underloading_3;


/*
I'm really confused that there is no requirements about indexes as it was 
in the task "SQL Foundation DDL" too. I don't know what have to think.  
Postgress automatically creates indexes on primary keys and unique CONSTRAINTS. 
So, it will be better TO create indexes AS minimum ON FOREIGN keys.


CREATE INDEX FK_ihc_inst_ID_idx ON workflow.institution_historical_capacity (inst_ID);

CREATE INDEX FK_cap_inst_ID_idx ON workflow.capability_potencial (inst_ID);

CREATE INDEX FK_cap_ca_ID_idx ON workflow.capability_potencial (ca_ID);

CREATE INDEX FK_s_p_ID_idx ON workflow.staff (p_ID);

CREATE INDEX FK_s_p_inst_ID ON workflow.staff (inst_ID);

CREATE INDEX FK_s_pos_ID_idx ON workflow.staff (pos_ID);

CREATE INDEX FK_v_p_ID_idx ON workflow.visit (p_ID);

CREATE INDEX FK_v_s_ID_idx ON workflow.visit (s_ID);

CREATE INDEX FK_v_ca_ID_idx ON workflow.visit (ca_ID); */
