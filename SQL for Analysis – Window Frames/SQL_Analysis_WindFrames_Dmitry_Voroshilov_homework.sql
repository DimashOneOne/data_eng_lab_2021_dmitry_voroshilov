-- Task 1

-- Analyze annual sales by channels and regions

WITH cte AS (SELECT country_region,
			        calendar_year,
			        channel_desc,
			        sum(amount_sold) AS amount_sold,
			        sum(amount_sold) / sum(sum(amount_sold)) OVER (PARTITION BY country_region, calendar_year) AS ratio_amount_sold,
--                  max(sum(amount_sold)) OVER (PARTITION BY country_region, channel_desc ORDER BY calendar_year ROWS BETWEEN 1 PRECEDING AND CURRENT ROW EXCLUDE CURRENT ROW) /  -- as alternative variant
			        first_value(sum(amount_sold)) OVER (PARTITION BY country_region, channel_desc ORDER BY calendar_year ROWS BETWEEN 1 PRECEDING AND CURRENT ROW) /
			        sum(sum(amount_sold)) OVER (PARTITION BY country_region ORDER BY calendar_year GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW EXCLUDE GROUP) AS prev_ratio_amount_sold
			 FROM sh.sales
			 INNER JOIN sh.customers USING (cust_id)
			 INNER JOIN sh.countries USING (country_id)
			 INNER JOIN sh.times USING (time_id)
			 INNER JOIN sh.channels USING (channel_id)
		 	 WHERE country_region IN ('Americas', 'Asia', 'Europe')
			 GROUP BY country_region, calendar_year, channel_id, channel_desc
			 ORDER BY country_region, calendar_year, channel_desc)
SELECT country_region,
	   calendar_year,
	   channel_desc, 
       amount_sold::int || ' $' AS amount_sold,
       round(ratio_amount_sold * 100, 2) || ' %' AS  "% BY CHANNELS",
       round(prev_ratio_amount_sold * 100, 2) || ' %' AS "% BY PREVIOUS PERIOD",
       round((ratio_amount_sold - prev_ratio_amount_sold)*100, 2) || ' %' AS "% DIFF"
FROM cte
WHERE calendar_year IN (1999, 2000, 2001);


-- Task 2

/*
Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. Add column CUM_SUM for accumulated amounts within 
weeks. For each day, display the average sales for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column). 
For Monday, calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for Thursday + Friday + weekends
*/

SELECT calendar_week_number,
	   time_id,
	   day_name,
       sum(amount_sold) AS sales,
       sum(sum(amount_sold)) OVER (PARTITION BY calendar_week_number ORDER BY time_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cum_sum,
       CASE WHEN day_name = 'Friday'
            THEN round(avg(sum(amount_sold)) OVER (ORDER BY time_id ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING), 2)
            WHEN day_name = 'Monday'
            THEN round(avg(sum(amount_sold)) OVER (ORDER BY time_id ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING), 2)
            ELSE round(avg(sum(amount_sold)) OVER ( ORDER BY time_id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING), 2)
       END AS centered_3_day_avg
FROM sh.sales
INNER JOIN sh.times USING (time_id)
WHERE calendar_week_number IN (49, 50, 51) AND 
      calendar_year = 1999
GROUP BY calendar_week_number, time_id, day_name;


-- Task 3

/* For weeks 46, 47, 48, 51, 52 in 1999, 2000, calculate the total revenue for each pair of weeks, 
and the average total revenue of the current and previous weeks, considering and not considering vacations 
in weeks 49 and 50, when there were no sales */

WITH cte AS (SELECT calendar_year,
			       calendar_week_number,	   
			       sum(amount_sold) AS sales,
			       sum(sum(amount_sold)) OVER (ORDER BY calendar_week_number ROWS BETWEEN 1 PRECEDING AND CURRENT ROW) AS week_sum, -- I can't use here groups or range
			       avg(sum(amount_sold)) OVER (ORDER BY calendar_week_number GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW) AS avg_not_considering_vacations,
			       avg(sum(amount_sold)) OVER (ORDER BY calendar_week_number RANGE BETWEEN 1 PRECEDING AND CURRENT ROW) avg_considering_vacations -- for interruption calculation
			FROM sh.sales
			INNER JOIN sh.customers USING (cust_id)
			INNER JOIN sh.times USING (time_id)
			WHERE calendar_week_number IN (46, 47, 48, 51, 52) AND 
			      calendar_year IN (1999, 2000)
			GROUP BY calendar_year, calendar_week_number)
SELECT calendar_week_number,
       week_sum,
       avg_not_considering_vacations,
       avg_considering_vacations
FROM cte
WHERE calendar_year = 2000;



