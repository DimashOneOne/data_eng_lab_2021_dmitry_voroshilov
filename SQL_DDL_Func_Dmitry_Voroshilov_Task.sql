CREATE OR REPLACE FUNCTION public.task(IN client_id int, IN left_boundary date, IN right_boundary date, OUT "customer's_info" TEXT, OUT metric_name text)
RETURNS SETOF record
AS $$
SELECT 'customer''s info',
        concat(initcap(first_name), ' ', initcap(last_name), ', ', lower(email)) 
FROM public.customer
WHERE customer_id = $1
UNION ALL
SELECT 'num. of films rented' AS info,
        count(1)::text
FROM public.rental
WHERE (rental_date BETWEEN $2 AND $3) AND customer_id = $1
GROUP BY info, customer_id
UNION ALL
SELECT 'rented film''s titles' AS info,
string_agg(DISTINCT lower(title), ', ') -- I think it will be better to show only unique titles
FROM (SELECT customer_id,
             inventory_id
      FROM public.rental
      WHERE rental_date <= $3 AND customer_id = $1) t  -- This is at the end of specified time period. If it needs to filter during specified timeframe: BETWEEN $2 AND $3
INNER JOIN public.inventory USING (inventory_id)
INNER JOIN public.film USING (film_id)
GROUP BY info, customer_id
UNION ALL 
SELECT 'num. of payments' AS info,
        count(1)::TEXT
FROM public.payment
WHERE customer_id = $1 AND (payment_date BETWEEN $2 AND $3 )
GROUP BY info, customer_id
UNION ALL 
SELECT 'payments'' amount' AS info,
        sum(amount)::TEXT
FROM public.payment
WHERE customer_id = $1 AND (payment_date BETWEEN $2 AND $3)
GROUP BY info, customer_id
$$
LANGUAGE SQL;

-- SELECT * FROM public.task(269, '2000-01-01', '2019-01-01');

-- DROP FUNCTION task(integer,date,date);