-- Build the query to generate sales report for 1999 and 2000 in the context of quarters and product categories. 
-- In the report you should analyze the sales of products from the categories 'Electronics', 'Hardware' and 'Software/Other', through the channels 'Partners' and 'Internet'


SELECT calendar_year::char(4),
	   calendar_quarter_desc,
	   prod_category,
	   sum(amount_sold) AS "sales$",
	   CASE WHEN calendar_quarter_number = 1 
	        THEN 'N/A'     
	        ELSE round(sum(amount_sold) / 
	             FIRST_VALUE(sum(amount_sold)) OVER (PARTITION BY calendar_year, prod_category ORDER BY calendar_quarter_desc) * 100 - 100, 2) || '%' 
	       -- or NTH_VALUE (sum(amount_sold), 1) OVER (PARTITION BY calendar_year, prod_category ORDER BY calendar_quarter_desc) * 100 - 100, 2) || '%'     
	   END AS diff_percent,
	   sum(sum(amount_sold)) OVER w AS "cum_sum$"
FROM sh.sales
INNER JOIN sh.channels USING (channel_id)
INNER JOIN sh.products USING (prod_id)
INNER JOIN sh.times USING (time_id)
WHERE calendar_year IN (1999, 2000) AND 
	  prod_category IN ('Electronics', 'Hardware', 'Software/Other') AND
	  channel_desc IN ('Partners', 'Internet')
GROUP BY calendar_year, calendar_quarter_number, calendar_quarter_desc, prod_category
WINDOW w AS (PARTITION BY calendar_year ORDER BY calendar_quarter_desc GROUPS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
ORDER BY calendar_year, calendar_quarter_desc ASC, 
         "sales$" DESC;
       
        