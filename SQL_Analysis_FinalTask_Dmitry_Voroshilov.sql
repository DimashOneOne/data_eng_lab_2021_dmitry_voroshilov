-- Task 1
-- Build a query to create sales report that shows sales distribution by gender (F / M), 
-- marital status and by age (21-30 / 31-40 /41-50 / 51-60 years). Calculate the total sales based on the marital status of the customers.


WITH cte AS (SELECT cust_gender,
				    lower(cust_marital_status) AS cust_marital_status,  
				    sum(amount_sold) FILTER (WHERE calendar_year - cust_year_of_birth BETWEEN 21 AND 30) AS a2130,
				    sum(amount_sold) FILTER (WHERE calendar_year - cust_year_of_birth BETWEEN 31 AND 40) AS a3140,  
				    sum(amount_sold) FILTER (WHERE calendar_year - cust_year_of_birth BETWEEN 41 AND 50) AS a4150,  
				    sum(amount_sold) FILTER (WHERE calendar_year - cust_year_of_birth BETWEEN 51 AND 60) AS a5160
			 FROM sh.sales
			 INNER JOIN sh.customers USING (cust_id)
			 INNER JOIN sh.times USING (time_id)
			 WHERE calendar_year = 2000 AND 
			 	   lower(cust_marital_status) IN ('single','married')
			 GROUP BY cust_gender, lower(cust_marital_status))
SELECT cust_gender,
       cust_marital_status,
       a2130::varchar AS "21-30", -- here and bellow varchar to show zeros as in task
       a3140::varchar AS "31-40",
       a4150::varchar AS "41-50",
       a5160::varchar AS "51-60"
FROM cte
UNION ALL
SELECT '',
       CASE WHEN cust_marital_status = 'single'
            THEN 'Total for single'
            ELSE 'Total for married'
       END,
       sum(a2130)::varchar,
       sum(a3140)::varchar,
       sum(a4150)::varchar,
       sum(a5160)::varchar
FROM cte
GROUP BY cust_marital_status;


-- Task 2
-- Create an annual sales report broken down by calendar years and months. Annual sales for January of each year are presented in monetary units ($), 
-- the remaining months should contain the dynamics of sales in percent relative to January.

SELECT calendar_year::varchar,
       sum(amount_sold) FILTER (WHERE calendar_month_number = 1)::varchar AS "Jan $",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 2) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Feb %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 3) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Mar %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 4) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Apr %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 5) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "May %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 6) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Jun %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 7) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Jul %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 8) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Aug %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 9) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Sep %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 10) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Oct %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 11) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Nov %",
       round(sum(amount_sold) FILTER (WHERE calendar_month_number = 12) / sum(amount_sold) FILTER (WHERE calendar_month_number = 1) * 100 - 100, 2)::varchar AS "Dec %"
FROM sh.sales
INNER JOIN sh.times USING (time_id)
GROUP BY calendar_year;


-- Task 3  
-- Which products were the most expensive (MAX (Costs.Unit_Price)) in their product category each year (1998-2001)?

WITH cte AS (SELECT calendar_year,
				    prod_category_id,
				    prod_id,
				    prod_name,
			        max(unit_price) AS unit_max,
			        max(max(unit_price)) OVER (PARTITION BY calendar_year, prod_category_id) AS gr_max	
-- or               FIRST_VALUE(max(unit_price)) OVER (PARTITION BY calendar_year, prod_category_id ORDER BY max(unit_price) DESC) AS gr_max	
-- or               NTH_VALUE(max(unit_price), 1) OVER (PARTITION BY calendar_year, prod_category_id ORDER BY max(unit_price) DESC) AS gr_max		        
			 FROM sh.costs
			 INNER JOIN sh.products USING (prod_id)
			 INNER JOIN sh.times USING (time_id)
			 WHERE calendar_year BETWEEN 1998 AND 2001
			 GROUP BY calendar_year, prod_category_id, prod_id, prod_name)
SELECT prod_name
FROM cte 
WHERE unit_max = gr_max
GROUP BY prod_id, prod_name
HAVING count(1) = 4;

