-- Top-3 most selling movie categories of all time and total dvd rental income for each category. Only 
-- consider dvd rental customers from the USA.


SELECT name AS most_selling_movie_categories,
	   sum(amount) AS total_rental
FROM public.category 
INNER JOIN public.film_category USING (category_id)
INNER JOIN public.film USING (film_id)
INNER JOIN public.inventory USING (film_id)
INNER JOIN (SELECT amount, 
				   inventory_id, 
				   ren.customer_id 
	        FROM public.rental ren 
	        INNER JOIN public.payment USING (rental_id) ) t USING (inventory_id)
INNER JOIN public.customer USING (customer_id)
INNER JOIN public.address USING (address_id)
INNER JOIN public.city USING (city_id)
INNER JOIN public.country USING (country_id)
WHERE country = 'United States' 
GROUP BY category_id, name
ORDER BY total_rental DESC
LIMIT 3


-- For each client, display a list of horrors that he had ever rented (in one column, separated by 
-- commas), and the amount of money that he paid for it


SELECT first_name, 
       last_name, 
       sum(amount) AS ammount_of_money,
       string_agg(DISTINCT (title), ', ') AS horror_films
FROM public.category 
INNER JOIN public.film_category USING (category_id)
INNER JOIN public.film USING (film_id)
INNER JOIN public.inventory USING (film_id)
INNER JOIN public.rental USING (inventory_id)
INNER JOIN public.customer USING (customer_id)
INNER JOIN public.payment USING (customer_id)
WHERE name = 'Horror'
	  AND payment.rental_id = rental.rental_id
GROUP BY customer_id, first_name, last_name 





