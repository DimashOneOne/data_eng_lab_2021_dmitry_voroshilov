-- All comedy movies released between 2000 and 2004, alphabetica

SELECT title 
FROM public.category
INNER JOIN public.film_category USING (category_id)
INNER JOIN public.film USING (film_id)
WHERE (release_year BETWEEN 2000 AND 2004) AND
	  name = 'Comedy'
ORDER BY title; 


-- Revenue of every rental store for year 2017 (columns: address and address2 � as one column, revenue)

SELECT CASE WHEN address2 IS NOT NULL THEN concat(address, ' and ', address2)
       		ELSE address 
       		END AS adress_as_one_column,
	   sum(amount) AS revenue
FROM public.payment
INNER JOIN public.staff sta USING (staff_id)
INNER JOIN public.store sto ON sta.store_id = sto.store_id
INNER JOIN public.address ad ON ad.address_id = sto.address_id
WHERE EXTRACT(YEAR FROM payment_date) = 2017
GROUP BY sto.store_id, adress_as_one_column; 

-- Top-3 actors by number of movies they took part in (columns: first_name, last_name, number_of_movies, sorted by 
--number_of_movies in descending order)

SELECT first_name, 
	   last_name,
	   count(1) AS number_of_movies
FROM public.actor 
INNER JOIN public.film_actor USING (actor_id)
INNER JOIN public.film USING (film_id)
GROUP BY actor_id, first_name, last_name 
ORDER BY number_of_movies DESC
LIMIT 3;


-- Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies, 
-- number_of_horror_movies, number_of_comedy_movies), sorted by release year in descending order

SELECT release_year,
	   sum(CASE WHEN name = 'Action' THEN 1 ELSE 0 END) AS number_of_action_movies,
       sum(CASE WHEN name = 'Horror' THEN 1 ELSE 0 END) AS number_of_horror_movies,
	   sum(CASE WHEN name = 'Comedy' THEN 1 ELSE 0 END) AS number_of_comedy_movies
FROM public.category
INNER JOIN public.film_category USING (category_id)
INNER JOIN public.film USING (film_id)
WHERE name IN ('Comedy', 'Horror', 'Action') 
GROUP by release_year
ORDER BY release_year DESC;


-- Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?

SELECT best_stuff.* 									-- I defined names of columns in 'SELECT' under "CROSS JOIN LATERAL". So, I think that in this case can be used '*'. 
FROM store sto 
CROSS JOIN LATERAL (SELECT sta.store_id,
						   first_name,
			   		       last_name
			 	   FROM public.payment
				   INNER JOIN staff sta USING (staff_id)
				   WHERE EXTRACT(YEAR FROM payment_date) = 2017 AND 
				   		 sto.store_id = sta.store_id
				   GROUP BY store_id, staff_id, first_name, last_name
				   ORDER BY sum(amount) DESC
				   FETCH NEXT 1 ROWS WITH TIES) best_stuff;


-- Which 5 movies were rented more than others and what's expected audience age for those movies?

SELECT title, 
    	CASE rating 
			WHEN 'NC-17' THEN 'Only adults'
			WHEN 'R'     THEN 'Adults and children under 17 only with accompanying parent or adult guardian'
			WHEN 'PG-13' THEN 'Adults and children under 13'
			WHEN 'PG'    THEN 'Adults and children under parental guidance'
			WHEN 'G'     THEN 'All ages' 
			END expected_audience_age
FROM (SELECT film_id
	  FROM public.rental
	  INNER JOIN public.inventory USING (inventory_id)
	  GROUP BY film_id
	  ORDER BY count(1) DESC
	  LIMIT 5) max_rented
INNER JOIN public.film USING (film_id);
		

-- Which actors/actresses didn't act for a longer period of time than others?

WITH cte AS (SELECT film_id, 
				    release_year,
				    actor_id
			FROM public.film
			INNER JOIN film_actor USING (film_id))
SELECT first_name, 
	   last_name
FROM public.actor
WHERE actor_id IN (SELECT cte1.actor_id
				   FROM cte cte1	
				   CROSS JOIN LATERAL (SELECT cte2.release_year 
				   				       FROM cte cte2 
				   				       WHERE cte1.actor_id = cte2.actor_id AND 
				   				             cte1.release_year < cte2.release_year
				                       ORDER BY cte1.actor_id, cte2.release_year 
				                       LIMIT 1) next_year  
				   ORDER BY (next_year.release_year - cte1.release_year) DESC 
				   FETCH NEXT 1 ROWS WITH TIES);

-- Another variant

WITH cte0 AS (SELECT DISTINCT release_year,
				    		  actor_id
			  FROM public.film
			  INNER JOIN film_actor USING (film_id)),
	 cte  AS (SELECT actor_id,
	 				 release_year,
	 				 (SELECT count(1) FROM cte0 cte_t WHERE cte_t.actor_id = cte0.actor_id AND
	 									 				    cte_t.release_year < cte0.release_year) AS row_num
	 		  FROM cte0)
SELECT first_name, 
	   last_name
FROM public.actor
WHERE actor_id IN (SELECT actor_id
				   FROM cte 
				   INNER JOIN cte cte_t USING (actor_id) 
				   WHERE cte.row_num - cte_t.row_num = 1 
				   ORDER BY cte.release_year - cte_t.release_year DESC 
				   FETCH NEXT 1 ROWS WITH TIES);