CREATE TABLE airflights.airport (
	a_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	a_iata_code char(3) NOT NULL UNIQUE CHECK (char_length(trim(a_iata_code)) = 3),
	a_name var_names NOT NULL,
	al_id SMALLINT NOT NULL REFERENCES airflights.airport_location (al_id),
	EXCLUDE USING gist (a_iata_code WITH = , a_name WITH =) 
);

CREATE INDEX fk_airport_al_id ON airflights.airport (al_id);