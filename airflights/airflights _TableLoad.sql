CREATE GLOBAL TEMP TABLE airflights_copy (
	ARILINE_ICAO_CODE varchar NULL,
	ARILINE_NAME varchar NULL,
	ARILINE_CALL_SIGN varchar NULL,
	AIRLINE_COUNTRY varchar NULL,
	FLIGHT_CODE varchar NULL,
	AIRPORT_FROM_IATA_CODE varchar NULL,
	AIRPORT_FROM_NAME varchar NULL,
	AIRPORT_FROM_LOCATION varchar NULL,
	AIRPORT_TO_IATA_CODE varchar NULL,
	AIRPORT_TO_NAME varchar NULL,
	AIRPORT_TO_LOCATION varchar NULL,
	FLIGHT_DISTANCE SMALLINT NULL,
	PILOT_NAME varchar NULL,
	PILOT_SURNAME varchar NULL,
	COPILOT_NAME varchar NULL,
	COPILOT_SURNAME varchar NULL,
	PASSENGER_FIRST_NAME varchar NULL,
	PASSENGER_LAST_NAME varchar NULL,
	PASSENGER_COUNTRY varchar NULL,
	TRAVEL_CLASS varchar NULL,
	TICKET_CODE varchar NULL,
	TICKET_PRICE int NULL
);

COPY airflights_copy FROM 'D:\\airflights.csv' DELIMITER ';' CSV HEADER;

CREATE GLOBAL TEMP TABLE array_location AS
SELECT string_to_array(AIRPORT_FROM_LOCATION, ',') AS t_arr 
FROM airflights_copy
UNION
SELECT string_to_array(AIRPORT_TO_LOCATION, ',') 
FROM airflights_copy;

-- table country
/*
CREATE TABLE airflights.country (
	c_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	c_name var_names NOT NULL UNIQUE
);*/

INSERT INTO airflights.country (c_name)
SELECT CASE WHEN CARDINALITY(t_arr) = 4 THEN trim(t_arr[4])
            WHEN CARDINALITY(t_arr) = 3 THEN trim(t_arr[3])
            ELSE trim(t_arr[2])
            END AS c_name
FROM array_location
UNION
SELECT PASSENGER_COUNTRY
FROM airflights_copy
WHERE PASSENGER_COUNTRY IS NOT NULL
UNION
SELECT AIRLINE_COUNTRY
FROM airflights_copy;


-- table airport_location
/*
CREATE TABLE airflights.airport_location (
	al_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	al_name var_names NOT NULL,
	c_id SMALLINT NOT NULL REFERENCES airflights.country (c_id),
	UNIQUE (al_name, c_id)
);

CREATE INDEX fk_airport_location_c_id ON airflights.airport_location (c_id);
*/
INSERT INTO airflights.airport_location (al_name, c_id)
SELECT CASE WHEN CARDINALITY(t_arr) = 4 THEN trim(array_to_string(t_arr[1:3], ','))
            WHEN CARDINALITY(t_arr) = 3 THEN trim(array_to_string(t_arr[1:2], ','))
            ELSE t_arr[1]
            END AS al_name,
            c_id
FROM array_location LEFT JOIN airflights.country ON c_name = CASE WHEN CARDINALITY(t_arr) = 4 THEN trim(t_arr[4])
                                                                  WHEN CARDINALITY(t_arr) = 3 THEN trim(t_arr[3])
                                                                  ELSE trim(t_arr[2])
                                                                  END;


-- table airport                                                                  
/*                                                                  
CREATE TABLE airflights.airport (
	a_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	a_iata_code char(3) NOT NULL UNIQUE CHECK (char_length(trim(a_iata_code)) = 3),
	a_name var_names NOT NULL,
	al_id SMALLINT NOT NULL REFERENCES airflights.airport_location (al_id),
	EXCLUDE USING gist (a_iata_code WITH = , a_name WITH =) 
);

CREATE INDEX fk_airport_al_id ON airflights.airport (al_id);
*/
INSERT INTO airflights.airport (a_iata_code, a_name, al_id)
WITH cte AS (SELECT AIRPORT_FROM_IATA_CODE AS a_iata_code,
                    AIRPORT_FROM_NAME AS a_name, 
                    trim(AIRPORT_FROM_LOCATION) AS full_location -- It was a problem with one of the location, which had whitespace after value
            FROM airflights_copy
            UNION
            SELECT AIRPORT_TO_IATA_CODE,
                   AIRPORT_TO_NAME,
                   trim(AIRPORT_TO_LOCATION)
            FROM airflights_copy),
    cte2 AS (SELECT concat(al_name, ', ', c_name) AS full_location,
                    al_id
             FROM airflights.airport_location 
             INNER JOIN airflights.country USING (c_id))
SELECT a_iata_code,
       a_name,
       al_id
FROM cte 
INNER JOIN cte2 USING (full_location);


-- table flight_direction
/*
CREATE TABLE airflights.flight_direction (
	fd_id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	fd_code char(6) NOT NULL UNIQUE CHECK (char_length(trim(fd_code)) = 6),
	fd_airport_from SMALLINT NOT NULL REFERENCES airflights.airport (a_id),
	fd_airport_to SMALLINT NOT NULL REFERENCES airflights.airport (a_id),
	fd_distance SMALLINT NOT NULL CHECK (fd_distance > 10),
	-- CHECK (fd_airport_from != fd_airport_to), It's very intetesting that BBD276 flight has the same from and to airport. So, I turned off this check
	EXCLUDE USING gist (fd_code WITH = , fd_airport_from WITH =, fd_airport_to WITH =) 
);

CREATE INDEX fk_flight_direction_fd_airport_from ON airflights.flight_direction (fd_airport_from);

CREATE INDEX fk_flight_direction_fd_airport_to ON airflights.flight_direction (fd_airport_to);
*/
INSERT INTO airflights.flight_direction (fd_code, fd_airport_from, fd_airport_to, fd_distance)
WITH cte AS (SELECT DISTINCT FLIGHT_CODE,
			                 AIRPORT_FROM_IATA_CODE,
						  	 AIRPORT_TO_IATA_CODE,
							 FLIGHT_DISTANCE
			 FROM airflights_copy)
SELECT FLIGHT_CODE AS fd_code,
	   ai1.a_id AS fd_airport_from,
	   ai2.a_id AS fd_airport_to,
	   FLIGHT_DISTANCE AS fd_distance
FROM cte 
INNER JOIN airflights.airport ai1 ON AIRPORT_FROM_IATA_CODE = ai1.a_iata_code
INNER JOIN airflights.airport ai2 ON AIRPORT_TO_IATA_CODE = ai2.a_iata_code;


-- table airline        
/*        
CREATE TABLE airflights.airline (
	aln_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	aln_icao_code char(3) NOT NULL UNIQUE CHECK (char_length(trim(aln_icao_code)) = 3),
	aln_name var_names NOT NULL,
	aln_call_sign var_names NOT NULL,
	c_id SMALLINT NOT NULL REFERENCES airflights.country (c_id),
	EXCLUDE USING gist (aln_icao_code WITH = , aln_name WITH =),
	EXCLUDE USING gist (aln_name WITH = , aln_call_sign WITH =) 
);

CREATE INDEX fk_airline_c_id ON airflights.airline (c_id);
*/
INSERT INTO airflights.airline (aln_icao_code, aln_name, aln_call_sign, c_id)
WITH cte AS (SELECT DISTINCT ARILINE_ICAO_CODE,
			                 ARILINE_NAME,
			                 ARILINE_CALL_SIGN,
			                 AIRLINE_COUNTRY AS c_name
			 FROM airflights_copy)
SELECT ARILINE_ICAO_CODE AS aln_icao_code,
       ARILINE_NAME AS aln_name,
       ARILINE_CALL_SIGN AS aln_call,
       c_id
FROM cte 
INNER JOIN airflights.country USING (c_name);			 


-- table class
/*
CREATE TABLE airflights.class (
	cl_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	cl_name var_names NOT NULL UNIQUE
);
*/
INSERT INTO airflights.class (cl_name) 
SELECT DISTINCT TRAVEL_CLASS cl_name
FROM airflights_copy
WHERE TRAVEL_CLASS IS NOT null;


-- table flight
/*
CREATE TABLE airflights.flight (
	f_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	aln_id SMALLINT NOT NULL REFERENCES airflights.airline (aln_id),
	fd_id int NOT NULL REFERENCES airflights.flight_direction (fd_id),
	f_date TIMESTAMP NULL CHECK (f_date >= '1950-01-01 00:00:00'), -- This field is my hypothesis about the db model. Not null will be better, dut I haven't data
	EXCLUDE USING gist (aln_id WITH = , fd_id WITH =, f_date WITH =) 
);

CREATE INDEX fk_flight_aln_id ON airflights.flight (aln_id);

CREATE INDEX fk_flight_fd_id ON airflights.flight (fd_id);
*/
INSERT INTO airflights.flight (aln_id, fd_id)
SELECT aln_id,
	   fd_id
FROM (SELECT DISTINCT FLIGHT_CODE AS fd_code,
                      ARILINE_ICAO_CODE AS aln_icao_code
      FROM airflights_copy) t
      INNER JOIN airflights.flight_direction USING (fd_code)
      INNER JOIN airflights.airline USING (aln_icao_code);


-- table price    
/*
CREATE TABLE airflights.price (
	f_id bigint NOT NULL REFERENCES airflights.flight (f_id),
	cl_id SMALLINT NOT NULL REFERENCES airflights.class (cl_id),
	pr_value numeric(9,2) NOT NULL CHECK (pr_value >= 0),
	PRIMARY KEY (f_id, cl_id)
);

CREATE INDEX fk_price_f_id ON airflights.price (f_id);

CREATE INDEX fk_price_cl_id ON airflights.price (cl_id);
*/
INSERT INTO airflights.price (f_id, cl_id, pr_value)
WITH cte AS (SELECT f_id,
					fd_code,    
					aln_icao_code
			 FROM airflights.flight 
			 INNER JOIN airflights.flight_direction USING (fd_id)
			 INNER JOIN airflights.airline USING (aln_id))
SELECT f_id,
       cl_id,
       TICKET_PRICE AS pr_value
FROM (SELECT DISTINCT FLIGHT_CODE AS fd_code,
                      ARILINE_ICAO_CODE AS aln_icao_code,
                      TRAVEL_CLASS AS cl_name,
                      TICKET_PRICE
      FROM airflights_copy
      WHERE TICKET_PRICE IS NOT NULL) t
INNER JOIN airflights.class USING (cl_name)
INNER JOIN cte USING (fd_code, aln_icao_code);   -- I understand that this is redundant. But it is not redundant for my hypothesis about how db model should be


-- table status      
/*
CREATE TABLE airflights.status (
	st_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	st_name var_names NOT NULL UNIQUE
);
*/
INSERT INTO airflights.status (st_name) 
VALUES ('PILOT'),
       ('COPILOT'),
       ('PASSENGER');


-- table people      
/*
CREATE TABLE airflights.people (
	p_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	p_name var_names NOT NULL,
	p_surname var_names NOT NULL,
	c_id SMALLINT NULL REFERENCES airflights.country (c_id)
);

CREATE INDEX fk_people_c_id ON airflights.people (c_id);
*/
INSERT INTO airflights.people (p_name, p_surname, c_id)
WITH cte AS (SELECT PILOT_NAME AS PASSENGER_FIRST_NAME,
                    PILOT_SURNAME AS PASSENGER_LAST_NAME,
                    'staff' AS c_name 
             FROM airflights_copy
             UNION 
             SELECT COPILOT_NAME,
                    COPILOT_SURNAME,
                    'staff'
             FROM airflights_copy
             UNION ALL
             SELECT DISTINCT PASSENGER_FIRST_NAME,
                             PASSENGER_LAST_NAME,
                             PASSENGER_COUNTRY     
             FROM airflights_copy
             WHERE PASSENGER_LAST_NAME IS NOT NULL AND
             	   PASSENGER_FIRST_NAME IS NOT NULL)
SELECT PASSENGER_FIRST_NAME AS p_name,
	   PASSENGER_LAST_NAME AS p_surname,
	   country.c_id
FROM cte LEFT OUTER JOIN airflights.country USING (c_name);   


-- table people_on_flight
/*
CREATE TABLE airflights.people_on_flight (
	f_id bigint REFERENCES airflights.flight (f_id),
	p_id bigint REFERENCES airflights.people (p_id),
	st_id SMALLINT REFERENCES airflights.status,
	pof_ticket_code varchar(10) NULL CHECK (char_length(trim(pof_ticket_code)) > 0),
	cl_id SMALLINT NULL REFERENCES airflights.class (cl_id),
	PRIMARY KEY (f_id, p_id, st_id),
	EXCLUDE USING gist (p_id WITH = , pof_ticket_code WITH =) 
);

CREATE INDEX fk_people_on_flight_f_id ON airflights.people_on_flight (f_id );

CREATE INDEX fk_people_on_flight_p_id ON airflights.people_on_flight (p_id );

CREATE INDEX fk_people_on_flight_st_id ON airflights.people_on_flight (st_id );

CREATE INDEX fk_people_on_flight_cl_id ON airflights.people_on_flight (cl_id );
*/
INSERT INTO airflights.people_on_flight (f_id, p_id, pof_ticket_code, st_id, cl_id)
WITH cte AS (SELECT f_id,
					fd_code,    
					aln_icao_code
			 FROM airflights.flight INNER JOIN airflights.flight_direction USING (fd_id)
			                        INNER JOIN airflights.airline USING (aln_id))
SELECT f_id,
       p_id,
       pof_ticket_code,
       (SELECT st_id 
       FROM airflights.status 
       WHERE st_name = 'PASSENGER') AS st_id,
       cl_id
FROM (SELECT FLIGHT_CODE AS fd_code,
             ARILINE_ICAO_CODE AS aln_icao_code,
             TICKET_CODE AS pof_ticket_code,
             PASSENGER_FIRST_NAME AS p_name,
	         PASSENGER_LAST_NAME AS p_surname,
	         TRAVEL_CLASS AS cl_name
      FROM airflights_copy
      WHERE TICKET_CODE IS NOT NULL) t 
      INNER JOIN cte USING (fd_code, aln_icao_code)
      INNER JOIN airflights.people USING (p_name, p_surname)
      INNER JOIN airflights.class USING (cl_name); 

INSERT INTO airflights.people_on_flight (f_id, p_id, st_id)     
WITH cte AS (SELECT f_id,
					fd_code,    
					aln_icao_code
			 FROM airflights.flight INNER JOIN airflights.flight_direction USING (fd_id)
			                        INNER JOIN airflights.airline USING (aln_id))
SELECT  f_id,
        p_id,
        (SELECT st_id 
        FROM airflights.status 
        WHERE st_name = 'PILOT') AS st_id
FROM (SELECT DISTINCT FLIGHT_CODE AS fd_code,
                      ARILINE_ICAO_CODE AS aln_icao_code,
                      PILOT_NAME AS p_name,
	                  PILOT_SURNAME AS p_surname
      FROM airflights_copy) t 
      INNER JOIN cte USING (fd_code, aln_icao_code)
      INNER JOIN airflights.people USING (p_name, p_surname)
      WHERE c_id IS NULL; 

INSERT INTO airflights.people_on_flight (f_id, p_id, st_id)     
WITH cte AS (SELECT f_id,
					fd_code,    
					aln_icao_code
			 FROM airflights.flight INNER JOIN airflights.flight_direction USING (fd_id)
			                        INNER JOIN airflights.airline USING (aln_id))
SELECT  f_id,
        p_id,
        (SELECT st_id 
        FROM airflights.status 
        WHERE st_name = 'COPILOT') AS st_id
FROM (SELECT DISTINCT FLIGHT_CODE AS fd_code,
                      ARILINE_ICAO_CODE AS aln_icao_code,
                      COPILOT_NAME AS p_name,
	                  COPILOT_SURNAME AS p_surname
      FROM airflights_copy) t 
      INNER JOIN cte USING (fd_code, aln_icao_code)
      INNER JOIN airflights.people USING (p_name, p_surname)
      WHERE c_id IS NULL;       