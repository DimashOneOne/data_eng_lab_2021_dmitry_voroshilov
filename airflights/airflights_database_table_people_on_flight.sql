CREATE TABLE airflights.people_on_flight (
	f_id bigint REFERENCES airflights.flight (f_id),
	p_id bigint REFERENCES airflights.people (p_id),
	st_id SMALLINT REFERENCES airflights.status,
	pof_ticket_code varchar(10) NULL CHECK (char_length(trim(pof_ticket_code)) > 0),
	cl_id SMALLINT NULL REFERENCES airflights.class (cl_id),
	PRIMARY KEY (f_id, p_id, st_id),
	EXCLUDE USING gist (p_id WITH = , pof_ticket_code WITH =) 
);

CREATE INDEX fk_people_on_flight_f_id ON airflights.people_on_flight (f_id );

CREATE INDEX fk_people_on_flight_p_id ON airflights.people_on_flight (p_id );

CREATE INDEX fk_people_on_flight_st_id ON airflights.people_on_flight (st_id );

CREATE INDEX fk_people_on_flight_cl_id ON airflights.people_on_flight (cl_id );