CREATE TABLE airflights.flight (
	f_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	aln_id SMALLINT NOT NULL REFERENCES airflights.airline (aln_id),
	fd_id int NOT NULL REFERENCES airflights.flight_direction (fd_id),
	f_date TIMESTAMP NULL CHECK (f_date >= '1950-01-01 00:00:00'), -- This field is my hypothesis about the db model. Not null will be better, dut I haven't data to fill 
	EXCLUDE USING gist (aln_id WITH = , fd_id WITH =, f_date WITH =) 
);

CREATE INDEX fk_flight_aln_id ON airflights.flight (aln_id);

CREATE INDEX fk_flight_fd_id ON airflights.flight (fd_id);