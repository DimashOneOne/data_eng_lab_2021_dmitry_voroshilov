CREATE TABLE airflights.people (
	p_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	p_name var_names NOT NULL,
	p_surname var_names NOT NULL,
	c_id SMALLINT NULL REFERENCES airflights.country (c_id)
);

CREATE INDEX fk_people_c_id ON airflights.people (c_id);
