CREATE TABLE airflights.airport_location (
	al_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	al_name var_names NOT NULL,
	c_id SMALLINT NOT NULL REFERENCES airflights.country (c_id),
	UNIQUE (al_name, c_id)
);

CREATE INDEX fk_airport_location_c_id ON airflights.airport_location (c_id);