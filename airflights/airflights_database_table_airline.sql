CREATE TABLE airflights.airline (
	aln_id SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	aln_icao_code char(3) NOT NULL UNIQUE CHECK (char_length(trim(aln_icao_code)) = 3),
	aln_name var_names NOT NULL,
	aln_call_sign var_names NOT NULL,
	c_id SMALLINT NOT NULL REFERENCES airflights.country (c_id),
	EXCLUDE USING gist (aln_icao_code WITH = , aln_name WITH =),
	EXCLUDE USING gist (aln_name WITH = , aln_call_sign WITH =) 
);

CREATE INDEX fk_airline_c_id ON airflights.airline (c_id);