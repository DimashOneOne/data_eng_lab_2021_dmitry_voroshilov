CREATE TABLE airflights.flight_direction (
	fd_id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	fd_code char(6) NOT NULL UNIQUE CHECK (char_length(trim(fd_code)) = 6),
	fd_airport_from SMALLINT NOT NULL REFERENCES airflights.airport (a_id),
	fd_airport_to SMALLINT NOT NULL REFERENCES airflights.airport (a_id),
	fd_distance SMALLINT NOT NULL CHECK (fd_distance > 10),
	-- CHECK (fd_airport_from != fd_airport_to), It's very intetesting that BBD276 flight has the same from and to airport. So, I turned off this check
	EXCLUDE USING gist (fd_code WITH = , fd_airport_from WITH =, fd_airport_to WITH =) 
);

CREATE INDEX fk_flight_direction_fd_airport_from ON airflights.flight_direction (fd_airport_from);

CREATE INDEX fk_flight_direction_fd_airport_to ON airflights.flight_direction (fd_airport_to);