-- 1. Calculate the citizens of which country fly the most (draw out the first 10 countries sorted by the decreasing number of flights)

SELECT c_name AS country, 
	   count(1) AS count
FROM airflights.country 
INNER JOIN airflights.people USING (c_id)
INNER JOIN airflights.people_on_flight USING (p_id)
GROUP BY c_id, c_name
ORDER BY count DESC 
LIMIT 10


-- 2. The airlines of which countries have made the largest number of flights (to withdraw 5 countries in descending order of the number of 
-- flights

SELECT c_name AS country, 
	   count(1) AS count
FROM airflights.country 
INNER JOIN airflights.airline USING (c_id)
INNER JOIN airflights.flight USING (aln_id)
GROUP BY c_id, c_name
ORDER BY count DESC
LIMIT 5;


-- 3. Which flights are more frequent: passenger or cargo

WITH cte AS (SELECT count(DISTINCT f_id)*1.0 AS pas
             FROM airflights.people_on_flight 
             INNER JOIN airflights.status USING (st_id) 
             WHERE st_name = 'PASSENGER'),
    cte2 AS (SELECT count(f_id) / 2.0 AS half
             FROM airflights.flight)             
SELECT CASE WHEN (SELECT pas FROM cte) > (SELECT half FROM cte2)
            THEN 'There are more passenger flights than cargo flights'
            WHEN (SELECT pas FROM cte) < (SELECT half FROM cte2)
            THEN 'There are more flights cargo than passenger flights'
            ELSE 'There are equal quantity of cargo and passenger flights'
            END AS result_of_task_3;

 
-- 4. Calculate the percentage of cargo and passenger transport for different airlines

WITH cte AS (SELECT aln_name, 
			        round(sum (CASE WHEN NOT EXISTS (SELECT 1 
			                                         FROM airflights.people_on_flight pof
				 		                             INNER JOIN airflights.status USING (st_id) 
				 		                             WHERE st_name = 'PASSENGER' AND 
						                                   f1.f_id = pof.f_id)
						           THEN 1
						           ELSE 0
						       END)*100.0 / count(1), 2) AS cargo 
			FROM airflights.flight f1 
			INNER JOIN airflights.airline USING (aln_id)
			GROUP BY aln_id, aln_name)
SELECT aln_name,
       cargo AS "percentage of cargo transport",
       100 - cargo AS "percentage of passenger transport"
FROM cte;

           
-- 5. Find the people who travel most frequently (draw out the first 20 c� indicating the number of flights, the total kilometrage and the total 
-- cost of all tickets of this passenger)

SELECT concat(p_name, ' ',  p_surname) AS name, 
       count(1) AS "the number of flights", 
       sum(fd_distance) AS "the total kilometrage", 
       sum(pr_value) AS "the total cost of all tickets"
FROM (SELECT f_id,
             p_id,
             cl_id
      FROM airflights.people_on_flight 
      INNER JOIN airflights.status USING (st_id) 
      WHERE st_name = 'PASSENGER') t
INNER JOIN airflights.people USING (p_id)
INNER JOIN airflights.flight USING (f_id)
INNER JOIN airflights.price USING (f_id, cl_id)
INNER JOIN airflights.flight_direction USING (fd_id)
GROUP BY p_id, p_name, p_surname
ORDER BY "the number of flights" DESC
LIMIT 20;
             

-- 6. Find people who have flown the most kilometers with some airline. How to better represent (visualize) this information

SELECT aln_name,
       concat(p_name, ' ',  p_surname) AS name,
       sum(fd_distance) AS "the total kilometrage"
FROM (SELECT f_id,
             p_id
      FROM airflights.people_on_flight 
      INNER JOIN airflights.status USING (st_id) 
      WHERE st_name = 'PASSENGER') t
INNER JOIN airflights.people USING (p_id)
INNER JOIN airflights.flight USING (f_id)
INNER JOIN airflights.airline USING (aln_id)
INNER JOIN airflights.flight_direction USING (fd_id)
GROUP BY aln_id, aln_name, p_id, p_name, p_surname
ORDER BY "the total kilometrage" DESC
FETCH NEXT 1 ROWS WITH TIES;


-- 7. Update the data with 15% discount on airfares for these (see above) passengers in this company

-- My existing model determines the price depending on a particular flight and class. I can't assign a discount to a specific passenger. 
-- So I should to change the database like this.

CREATE TABLE airflights.discount (
    d_id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY, 
	aln_id SMALLINT NOT NULL REFERENCES airflights.airline (aln_id),  -- There is no need to add the requirement of uniqueness to the combination aln_id and p_id or use them as primary key
	p_id bigint NOT NULL REFERENCES airflights.people (p_id),         -- So it is possible to have different discount programs for the same person from the same company
	d_value numeric(3,2) NOT NULL DEFAULT 1.00 CHECK (d_value BETWEEN 0 AND 1),
	d_date_from date NOT NULL CHECK (d_date_from >= '2000-01-01') DEFAULT current_date,
	d_date_to date NOT NULL CHECK (d_date_from >= '2000-01-01') DEFAULT current_date + INTERVAL '150 years', -- For passengers with discount for life
	CHECK (d_date_from <= d_date_to)	
);

CREATE INDEX fk_discount_aln_id ON airflights.discount (aln_id);

CREATE INDEX fk_discount_p_id ON airflights.discount (p_id);

WITH cte AS (INSERT INTO airflights.discount (aln_id, p_id, d_value)
			SELECT aln_id,
			       p_id,
			       0.15::numeric(3,2)
			FROM (SELECT f_id,
			             p_id
			      FROM airflights.people_on_flight 
			      INNER JOIN airflights.status USING (st_id) 
			      WHERE st_name = 'PASSENGER') t
			INNER JOIN airflights.people USING (p_id)
			INNER JOIN airflights.flight USING (f_id)
			INNER JOIN airflights.airline USING (aln_id)
			INNER JOIN airflights.flight_direction USING (fd_id)
			GROUP BY aln_id, p_id
			ORDER BY sum(fd_distance) DESC
			FETCH NEXT 1 ROWS WITH TIES
			RETURNING aln_id, p_id)
SELECT * -- I've defined names of columns in cte
INTO TEMP aln_id_p_id
FROM cte;

SELECT p_name || ' ' || p_surname AS passenger,
       pof_ticket_code, 
       pr_value AS "cost without discont",
       CASE WHEN dis.p_id IS NOT NULL AND (f_date BETWEEN d_date_from AND d_date_to) -- You can comment after NULL to test this query
            THEN pr_value * (1 - d_value)
            ELSE pr_value 
            END AS "cost with discont"
FROM airflights.people_on_flight 
INNER JOIN airflights.flight USING (f_id)
INNER JOIN aln_id_p_id USING (aln_id, p_id)
INNER JOIN airflights.price USING (f_id, cl_id)
INNER JOIN airflights.people USING (p_id)
LEFT JOIN airflights.discount dis USING (aln_id, p_id);

-- 8. Delete from the database information about passengers who were registered for less than 5 flights

ALTER TABLE airflights.people_on_flight DROP CONSTRAINT people_on_flight_p_id_fkey;

ALTER TABLE airflights.people_on_flight ADD CONSTRAINT people_on_flight_p_id_fkey FOREIGN KEY (p_id) 
REFERENCES airflights.people (p_id) ON DELETE CASCADE;

DELETE FROM airflights.people
WHERE p_id = ANY (SELECT p_id 
			      FROM airflights.people_on_flight
		          INNER JOIN airflights.status USING (st_id) 
		          WHERE st_name = 'PASSENGER'
			      GROUP BY p_id
			      HAVING count(1) < 5)
RETURNING *;


-- 9. Add a passenger flight operated by �Belavia Belarusian Airlines� on the route �Minsk International Airport� � �Warsaw, Poland� (57 
-- passengers are registered for the flight

WITH cte AS (SELECT (SELECT concat(aln_icao_code,(random()*1000)::int)
			        FROM airflights.airline
			        WHERE aln_name = 'Belavia Belarusian Airlines') AS fd_code,
					(SELECT a_id
					FROM airflights.airport
					WHERE a_name = 'Minsk International Airport') AS fd_airport_from,
					(SELECT a_id
					FROM airflights.airport
					INNER JOIN airflights.airport_location USING (al_id)
					INNER JOIN airflights.country USING (c_id)
					WHERE concat(al_name, ', ', c_name) = 'Warsaw, Poland'
					LIMIT 1) AS fd_airport_to,
				    (random()*1000)::smallint AS fd_distance),
    cte2 AS (INSERT INTO airflights.flight_direction (fd_code, fd_airport_from, fd_airport_to, fd_distance)
    	     SELECT *
    	     FROM cte
    	     WHERE NOT EXISTS (SELECT 1 
                               FROM airflights.flight_direction fd 
                               WHERE fd.fd_airport_from = cte.fd_airport_from AND
                                     fd.fd_airport_to = cte.fd_airport_to AND 
                                     substring(fd.fd_code from 1 for 3) = substring(cte.fd_code from 1 for 3))
             RETURNING fd_id),
     cte3 AS (SELECT fd_id 
		     FROM cte2  
			 UNION 
	         SELECT fd_id  
	         FROM airflights.flight_direction fd 
	         INNER JOIN cte ON fd.fd_airport_from = cte.fd_airport_from AND
                               fd.fd_airport_to = cte.fd_airport_to AND 
                               substring(fd.fd_code from 1 for 3) = substring(cte.fd_code from 1 for 3))
SELECT * -- I've defined names of columns in cte
INTO TEMP fd_id
FROM cte3;

WITH cte AS (INSERT INTO  airflights.flight (fd_id, aln_id)
			 VALUES ((SELECT * 
			         FROM fd_id),
			        (SELECT aln_id
				     FROM airflights.airline
				     WHERE aln_name = 'Belavia Belarusian Airlines'))
			 RETURNING f_id)
SELECT * -- I've defined names of columns in cte
INTO TEMP f_id
FROM cte;

INSERT INTO airflights.people_on_flight (f_id, p_id, st_id)
SELECT (SELECT f_id 
        FROM f_id),
        p_id,
        (SELECT st_id
        FROM airflights.status
        WHERE st_name = 'PASSENGER')
FROM airflights.people 
LIMIT 57
RETURNING *;

DO 
$$
DECLARE 
tmp record;
BEGIN 
SELECT f_id,
       fd_code INTO tmp
FROM (SELECT fd_id,
			 fd_code
      FROM airflights.flight_direction
      WHERE fd_airport_from = (SELECT a_id
      					       FROM airflights.airport
      					       WHERE a_name = 'Minsk International Airport') AND 
      	    fd_airport_to = (SELECT a_id
      	                     FROM airflights.airport
					         INNER JOIN airflights.airport_location USING (al_id)
					         INNER JOIN airflights.country USING (c_id)
					         WHERE concat(al_name, ', ', c_name) = 'Warsaw, Poland'
					         LIMIT 1)) t
INNER JOIN airflights.flight USING (fd_id)
INNER JOIN airflights.airline USING (aln_id)
WHERE aln_name = 'Belavia Belarusian Airlines'  
ORDER BY f_id DESC
LIMIT 1;

UPDATE airflights.people_on_flight
SET pof_ticket_code = tmp.fd_code || '/' || (random()*1000)::int,
    cl_id = (SELECT cl_id
             FROM airflights.class
             LIMIT 1)
WHERE f_id = tmp.f_id AND  
      p_id = ANY (SELECT p_id
			      FROM airflights.people_on_flight
                  INNER JOIN airflights.status USING (st_id)
                  WHERE st_name = 'PASSENGER');

INSERT INTO airflights.price (f_id, cl_id, pr_value)
SELECT tmp.f_id,
       cl_id,
       random()*1000
FROM airflights.class;
END
$$ LANGUAGE plpgsql
;

/* for testing

SELECT *
FROM airflights.people_on_flight
INNER JOIN airflights.status USING (st_id)
WHERE f_id = 274 AND st_name = 'PASSENGER';

SELECT *
FROM airflights.price
WHERE f_id = 274;

*/



