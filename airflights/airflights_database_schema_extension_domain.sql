CREATE DATABASE airflights OWNER postgres;

CREATE SCHEMA IF NOT EXISTS airflights;

CREATE EXTENSION btree_gist; -- installation of the additional module gist

DROP SCHEMA airflights CASCADE;

CREATE DOMAIN var_names AS varchar(150) CHECK (char_length(trim(value)) > 0);

DROP DOMAIN var_names CASCADE;

DROP DATABASE airflights;