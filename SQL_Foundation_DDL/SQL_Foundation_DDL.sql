/*� Create all tables for the relational model you created while studying DB Basics module (fixed model in 3nf). Create a separate database 
for them and give it appropriate domain-related name. Make sure you choose optimal datatype for each column. Use NOT NULL 
constraints, default values and generated columns where appropriate.
� Create all table relationships with primary and foreign keys.
� Create at least 5 check constraints, not considering unique and not null, on your tables (in total 5, not for each table).*/


-- CREATE EXTENSION btree_gist; -- installation of the additional module gist


CREATE DATABASE the_alpinist_club OWNER postgres;


/*DROP SCHEMA chronicles_of_ascents CASCADE; */


CREATE SCHEMA chronicles_of_ascents;


CREATE DOMAIN var_names AS varchar(150) CHECK (char_length(trim(value)) > 0);  -- to prevent blank strings


CREATE TABLE chronicles_of_ascents.location_level (
	location_level_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	location_level_name var_names NOT NULL UNIQUE
);


CREATE TABLE chronicles_of_ascents.location (
	location_ID int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	location_parent_ID int NULL REFERENCES chronicles_of_ascents.location (location_ID),
	location_name var_names NOT NULL,
	location_level_ID SMALLINT NOT NULL REFERENCES chronicles_of_ascents.location_level (location_level_ID),
	CHECK ((location_level_ID = 1 AND location_parent_ID IS NULL) OR        -- to provide the higest level only for countries 
           (location_level_ID != 1 AND location_parent_ID IS NOT NULL)), 
    EXCLUDE USING gist (location_parent_ID WITH = , location_name WITH =)   -- as an experiment to prevent dublicates (might be realized by unique constraint)
);


CREATE TABLE chronicles_of_ascents.alpinist (
	alpinist_ID int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	alpinist_name var_names NOT NULL DEFAULT 'unknown', 
	location_ID int NULL REFERENCES chronicles_of_ascents.location(location_ID)
);


CREATE TABLE chronicles_of_ascents.mountain (
	mountain_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	mountain_name var_names NOT NULL
);


CREATE TABLE chronicles_of_ascents.peak (
	peak_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	peak_name var_names NOT NULL,
	peak_height SMALLINT NOT NULL CHECK (peak_height BETWEEN 300 AND 8900),
	peak_height_f SMALLINT GENERATED ALWAYS AS (peak_height * 3.28) STORED, -- as an experiment. This field wasn't in the original db model
	mountain_ID SMALLINT NULL REFERENCES chronicles_of_ascents.mountain (mountain_ID),
	location_ID int NULL REFERENCES chronicles_of_ascents.location (location_ID),
	UNIQUE (mountain_ID, peak_name)
);


CREATE TABLE chronicles_of_ascents.climbing_group (
	climbing_group_ID SMALLINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	climbing_group_name var_names NULL,
	climbing_group_start_date date NULL CHECK (climbing_group_start_date >= '2021-04-25'),
	climbing_group_finish_date date NULL CHECK (climbing_group_start_date >= '2021-04-25'),
	peak_ID SMALLINT REFERENCES chronicles_of_ascents.peak (peak_ID),
	CHECK (climbing_group_start_date <= climbing_group_finish_date)
);


CREATE TABLE chronicles_of_ascents.m2m_climbing (
	climbing_group_ID SMALLINT REFERENCES chronicles_of_ascents.climbing_group (climbing_group_ID),
	alpinist_ID int REFERENCES chronicles_of_ascents.alpinist (alpinist_ID),
	m2m_climbing_success_indicator bit(1) NULL, -- or bit or boolean
	PRIMARY KEY (climbing_group_ID, alpinist_ID)
);


-- Fill your tables with sample data (create it yourself, 20+ rows total in all tables, make sure each table has at least 2 rows)


INSERT INTO chronicles_of_ascents.location_level (location_level_name)
VALUES ('COUNTRY'),
	   ('REGION');

	  
INSERT INTO chronicles_of_ascents.location (location_parent_id, location_name, location_level_id)
VALUES (null, 'BELARUS', 1),
	   (1, 'GOMELSKAY OBLAST', 2),
	   (1, 'MINSKAY OBLAST', 2),
	   (null, 'RUSSIA', 1);


INSERT INTO chronicles_of_ascents.alpinist (alpinist_name, location_id)
VALUES ('Gorec', 2),
	   (default, 1),
       (default, 2);
	  

INSERT INTO chronicles_of_ascents.mountain (mountain_name)
VALUES ('Himzavodskaya'),
       ('Medved gora'),
       ('Arart'),
       ('Olimp'),
       ('Holatchal');

      
INSERT INTO chronicles_of_ascents.peak (peak_name, peak_height, mountain_ID, location_ID)
VALUES ('Pyataya nasyp', 301, NULL, 2),
       ('Pereval Dyatlova', 1097, 5, 4),
       ('Vtoraya nasyp', 301, NULL, 2);

      
INSERT INTO chronicles_of_ascents.climbing_group (climbing_group_name, climbing_group_start_date, peak_ID)
VALUES ('Horzy', '2021-04-25', 1),
       (null, '2021-04-25', NULL);

      
INSERT INTO chronicles_of_ascents.m2m_climbing (climbing_group_ID, alpinist_ID, m2m_climbing_success_indicator)
VALUES (1, 1, '0'),
       (2, 2, NULL);      

      
-- Alter all tables and add 'record_ts' field to each table. Make it not null and set its default value to current_date. Check that the value 
-- has been set for existing rows      

      
ALTER TABLE chronicles_of_ascents.location_level ADD COLUMN record_ts date NOT NULL DEFAULT current_date;


ALTER TABLE chronicles_of_ascents.location ADD COLUMN record_ts date NOT NULL DEFAULT current_date;


ALTER TABLE chronicles_of_ascents.alpinist ADD COLUMN record_ts date NOT NULL DEFAULT current_date;


ALTER TABLE chronicles_of_ascents.mountain ADD COLUMN record_ts date NOT NULL DEFAULT current_date;

      
ALTER TABLE chronicles_of_ascents.peak ADD COLUMN record_ts date NOT NULL DEFAULT current_date;

      
ALTER TABLE chronicles_of_ascents.climbing_group ADD COLUMN record_ts date NOT NULL DEFAULT current_date;


ALTER TABLE chronicles_of_ascents.m2m_climbing ADD COLUMN record_ts date NOT NULL DEFAULT current_date;


SELECT CASE WHEN NOT EXISTS (SELECT record_ts 
							FROM chronicles_of_ascents.location_level
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.LOCATION
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.alpinist
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.mountain
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.peak
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.climbing_group
							WHERE record_ts IS NULL OR record_ts != current_date
							UNION
							SELECT record_ts 
							FROM chronicles_of_ascents.m2m_climbing
							WHERE record_ts IS NULL OR record_ts != current_date)
        THEN 'The current date value has been set for all existing rows' 
        ELSE 'Something wrong'
        END AS check_result;

       
-- An additional feature: indexes 

CREATE INDEX IDX_climbing_group_name ON chronicles_of_ascents.climbing_group (climbing_group_name varchar_pattern_ops);  -- I think, this is a good pattern for this field's type

CREATE INDEX IDX_climbing_group_dates ON chronicles_of_ascents.climbing_group (climbing_group_start_date, climbing_group_finish_date);

CREATE INDEX FK_climbing_group_peak_ID ON chronicles_of_ascents.climbing_group (peak_ID);

CREATE INDEX FK_peak_mountain_ID ON chronicles_of_ascents.peak (mountain_ID);

CREATE INDEX FK_peak_location_ID ON chronicles_of_ascents.peak (location_ID);

CREATE INDEX FK_alpinist_location_ID ON chronicles_of_ascents.alpinist (location_ID);

CREATE INDEX FK_location_location_level_ID ON chronicles_of_ascents.location (location_level_ID);

CREATE INDEX RFK_location_parent_ID ON chronicles_of_ascents.location (location_parent_ID);
  