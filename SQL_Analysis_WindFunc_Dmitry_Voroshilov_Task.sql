-- 1 Build the query to generate a report about regions with the maximum number of products sold (quantity_sold) for each channel for the entire 
-- period.

WITH cte AS (SELECT channel_id,
				   channel_desc,
				   country_region_id,
				   country_region,
				   sum(quantity_sold) AS sales,
				   round(sum(quantity_sold) / sum(sum(quantity_sold)) OVER (PARTITION BY channel_id) * 100, 2) AS sales_perc
			FROM sh.channels
			INNER JOIN sh.sales USING (channel_id)
			INNER JOIN sh.customers USING (cust_id)
			INNER JOIN sh.countries USING (country_id)
			GROUP BY channel_id, channel_desc, country_region_id, country_region),
cte2 AS (SELECT  channel_desc,
		      country_region,
		      sales,
		      sales_perc,
		      FIRST_VALUE(sales_perc) OVER (PARTITION BY channel_id ORDER BY sales_perc DESC) AS max_sales
              FROM cte)
SELECT  channel_desc,
		country_region,
		sales,
		sales_perc || '%' AS "SALES %"
FROM cte2
WHERE max_sales = sales_perc
ORDER BY channel_desc;


-- 2 Define subcategories of products (prod_subcategory) for which sales for 1998-2001 have always been higher (sum(amount_sold)) compared to 
-- the previous year. The final dataset must include only one column (prod_subcategory).


WITH cte AS    (SELECT prod_subcategory_id,
					   prod_subcategory,
					   calendar_year,
					   COALESCE(sum(amount_sold) - LAG(sum(amount_sold)) OVER (PARTITION BY prod_subcategory_id ORDER BY calendar_year), 0) AS def	   
				FROM sh.sales
				INNER JOIN sh.times USING (time_id)
				INNER JOIN sh.products USING(prod_id)
				GROUP BY prod_subcategory_id, prod_subcategory, calendar_year)
SELECT prod_subcategory
FROM cte 
WHERE def >= 0
GROUP BY prod_subcategory_id, prod_subcategory
HAVING count(1) = 4;

