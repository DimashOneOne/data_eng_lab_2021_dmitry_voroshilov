--  Implement role-based authentication model for dvd_rental database:
-- � Create group roles: DB developer, backend tester (read-only), customer (read-only for film and actor)

CREATE ROLE DB_developer WITH CREATEDB;

CREATE ROLE backend_tester;

CREATE ROLE customer;

-- � Create personalized role for any customer already existing in the dvd_rental database. Role name must be client_{first_name}_{last_name}
-- (omit curly brackets). Customer's payment and rental history must not be empty.

DO 
$$
DECLARE 
client varchar;
BEGIN 
SELECT trim(concat('client', '_', lower(first_name), '_', lower(last_name))) INTO client
FROM public.customer
INNER JOIN public.payment USING (customer_id)
INNER JOIN public.rental USING (customer_id)
LIMIT 1;
EXECUTE format('CREATE ROLE %s WITH IN ROLE customer LOGIN PASSWORD ''1234567'' VALID UNTIL ''2021-05-20''', client);
END 
$$ LANGUAGE plpgsql;

-- � Assign proper privileges to each role.

GRANT ALL ON DATABASE postgres TO DB_developer;

GRANT ALL ON ALL TABLES IN SCHEMA public TO DB_developer;

GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO DB_developer;

GRANT ALL ON ALL ROUTINES IN SCHEMA public TO DB_developer;

GRANT ALL ON LANGUAGE plpgsql TO DB_developer;

GRANT ALL ON SCHEMA public TO DB_developer;


GRANT CONNECT ON DATABASE postgres TO backend_tester;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO backend_tester;

GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO backend_tester;

GRANT EXECUTE ON ALL ROUTINES IN SCHEMA public TO backend_tester;

GRANT USAGE ON SCHEMA public TO backend_tester;


GRANT CONNECT ON DATABASE postgres TO customer;
 
GRANT SELECT ON TABLE public.film TO customer;

GRANT SELECT ON TABLE public.actor TO customer;


--  Verify that all roles are working as intended.

-- SELECT current_user;

-- customer

SET ROLE postgres;

SELECT country_id FROM public.country; -- permission denied

SELECT film_id FROM public.film; -- working

SELECT actor_id FROM public.actor; -- working

DELETE FROM public.film; -- permission denied

TRUNCATE public.film; -- permission denied

SELECT * FROM films_in_the_stock('tttest'); -- permission denied

SELECT * FROM actor_info; -- permission denied

CREATE DATABASE test; -- permission denied

CREATE SCHEMA test; -- permission denied


-- backend_tester

SET ROLE backend_tester;

SELECT country_id FROM public.country; -- working

DELETE FROM public.film; -- permission denied

TRUNCATE public.film; -- permission denied

INSERT INTO public.country (country)
VALUES ('test');   -- permission denied

DELETE FROM public.country
WHERE country = 'test';   -- permission denied

SELECT * FROM films_in_the_stock('tttest'); -- working

SELECT * FROM actor_info; -- working

CREATE DATABASE test; -- permission denied

CREATE SCHEMA test; -- permission denied


-- DB_developer

SET ROLE DB_developer;

SELECT country_id FROM public.country; -- working

INSERT INTO public.country (country)
VALUES ('test');   -- working

DELETE FROM public.country
WHERE country = 'test';   -- working

SELECT * FROM films_in_the_stock('tttest'); -- working

SELECT * FROM actor_info; -- working

CREATE DATABASE test; -- working

CREATE SCHEMA test; -- working