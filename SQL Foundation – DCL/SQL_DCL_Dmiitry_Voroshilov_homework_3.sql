-- Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) and configure it for your database, so that the 
-- customer can only access his own data in "rental" and "payment" tables (verify using the personalized role you previously created).

GRANT SELECT ON TABLE public.rental TO customer;

GRANT SELECT ON TABLE public.payment TO customer;

CREATE POLICY rental_policy ON public.rental AS RESTRICTIVE
FOR SELECT TO customer USING (customer_id IN (SELECT customer_id
                                             FROM public.customer
                                             WHERE upper(split_part(current_user, '_', 2)) = first_name AND 
                                             upper(split_part(current_user, '_', 3)) = last_name)); 

CREATE POLICY payment_policy ON public.payment AS RESTRICTIVE
FOR SELECT TO customer USING (customer_id IN (SELECT customer_id
                                             FROM public.customer
                                             WHERE upper(split_part(current_user, '_', 2)) = first_name AND 
                                             upper(split_part(current_user, '_', 3)) = last_name)); 
                                             

ALTER TABLE public.rental ENABLE ROW LEVEL SECURITY;

ALTER TABLE public.payment ENABLE ROW LEVEL SECURITY;

ALTER TABLE public.customer ENABLE ROW LEVEL SECURITY;

GRANT SELECT ON TABLE public.customer TO customer;

CREATE POLICY payment_policy_per ON public.payment FOR ALL USING(TRUE) WITH CHECK(TRUE);

CREATE POLICY customer_policy_per ON public.customer FOR ALL USING(TRUE) WITH CHECK(TRUE);

CREATE POLICY rental_policy_per ON public.rental FOR ALL USING(TRUE) WITH CHECK(TRUE);

CREATE POLICY customer_policy ON public.customer AS RESTRICTIVE
FOR SELECT TO customer USING (current_user = trim(concat('client', '_', lower(first_name), '_', lower(last_name)))); 
                                              
-- SELECT current_user;

-- SET ROLE postgres;

-- SET ROLE DB_developer;

-- SET ROLE client_patricia_johnson;

-- SELECT * FROM public.rental;

-- SELECT * FROM public.payment;

-- SELECT * FROM public.customer;