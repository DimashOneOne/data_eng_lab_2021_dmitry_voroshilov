/* How can one restrict access to certain columns of a database table?

 by defining Access Privileges (insert, select, update, references)

    What is the difference between user identification and user authentication?
    
Identification is the act of indicating of identity.
Authentication is the process by which the database server establishes the identity of the client, 
and by extension determines whether the client application (or the user who runs the client application) is permitted to connect with the database user name that was requested.

  What are the recommended authentication protocols for PostgreSQL?


Peer authentication is usually recommendable for local connections, though trust authentication might be sufficient in some circumstances. Password authentication is the easiest choice for remote connections.
All the other options require some kind of external security infrastructure (usually an authentication server or a certificate authority for issuing SSL certificates), or are platform-specific.


  What is proxy authentication in PostgreSQL and what is it for? Why does it make the previously discussed role-based access control easier to  implement?

Often, when designing an application, a login role is used to configure database connections and connection tools. Another level of security needs to be implemented to ensure that the user who uses the application is authorized to perform a certain task. 
This logic is often implemented in application business logic.
The database�s role system can also be used to partially implement this logic by delegating the authentication to another role after the connection is established or reused, using the SET SESSION AUTHORIZATION statement or SET ROLE command in a transaction block

Proxy authentication in PostgreSQ, it is assumed that we log in under one role, and then through, for example, set role or set session, we become another role with different PRIVILEGES

Proxy authentication requires more actions for the user

*/