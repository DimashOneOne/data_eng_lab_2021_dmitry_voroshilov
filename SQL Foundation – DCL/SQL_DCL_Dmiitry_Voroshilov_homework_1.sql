/* 1. Figure out what security precautions are already used in your 'dvd_rental' database;


All functions (with the exception of rewards_report) are executed with the rights of the calling user (INVOKER), and not with the rights of the user who created it (DEFINER).
The PL/pgSQL module is marked as trusted.
There are no special privileges and policies regarding tables and data types

*/
